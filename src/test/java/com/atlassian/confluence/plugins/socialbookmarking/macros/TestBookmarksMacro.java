package com.atlassian.confluence.plugins.socialbookmarking.macros;

import com.atlassian.confluence.languages.LocaleManager;
import com.atlassian.confluence.macro.Macro;
import com.atlassian.confluence.pages.Comment;
import com.atlassian.confluence.pages.Page;
import com.atlassian.confluence.pages.PageManager;
import com.atlassian.confluence.plugin.services.VelocityHelperService;
import com.atlassian.confluence.plugins.socialbookmarking.Bookmark;
import com.atlassian.confluence.plugins.socialbookmarking.BookmarkUtils;
import com.atlassian.confluence.plugins.socialbookmarking.Bookmarks;
import com.atlassian.confluence.spaces.Space;
import com.atlassian.confluence.spaces.SpaceManager;
import com.atlassian.confluence.util.i18n.I18NBean;
import com.atlassian.confluence.util.i18n.I18NBeanFactory;
import com.atlassian.plugin.webresource.WebResourceManager;
import com.atlassian.renderer.v2.RenderMode;
import com.atlassian.renderer.v2.macro.MacroException;
import junit.framework.TestCase;
import org.apache.commons.lang.StringUtils;
import org.hamcrest.BaseMatcher;
import org.hamcrest.Description;
import org.mockito.Matchers;
import org.mockito.Mock;
import org.mockito.MockitoAnnotations;
import org.mockito.invocation.InvocationOnMock;
import org.mockito.stubbing.Answer;

import java.util.Arrays;
import java.util.Collections;
import java.util.HashMap;
import java.util.LinkedHashSet;
import java.util.Locale;
import java.util.Map;
import java.util.Set;

import static org.mockito.Matchers.anyBoolean;
import static org.mockito.Matchers.anyInt;
import static org.mockito.Matchers.anyString;
import static org.mockito.Mockito.argThat;
import static org.mockito.Mockito.eq;
import static org.mockito.Mockito.never;
import static org.mockito.Mockito.verify;
import static org.mockito.Mockito.when;

public class TestBookmarksMacro extends TestCase
{
    @Mock
    private BookmarkUtils bookmarkUtils;

    @Mock
    private SpaceManager spaceManager;

    @Mock
    private PageManager pageManager;

    @Mock
    private LocaleManager localeManager;

    @Mock
    private I18NBeanFactory i18NBeanFactory;

    @Mock
    private I18NBean i18NBean;

    @Mock
    private WebResourceManager webResourceManager;

    @Mock
    private VelocityHelperService velocityHelperService;
    
    private BookmarksMacro bookmarksMacro;

    private Map<String, String> macroParams;

    private Space testSpace;

    private Page testPage;

    public void setUp() throws Exception
    {
        super.setUp();
        MockitoAnnotations.initMocks(this);

        when(i18NBeanFactory.getI18NBean(Matchers.<Locale>anyObject())).thenReturn(i18NBean);
        when(i18NBean.getText(anyString())).thenAnswer(new Answer<String>()
        {
            public String answer(InvocationOnMock invocationOnMock) throws Throwable
            {
                return invocationOnMock.getArguments()[0].toString();
            }
        });
        
        bookmarksMacro = new BookmarksMacro(bookmarkUtils, spaceManager, localeManager, i18NBeanFactory, webResourceManager, velocityHelperService);

        macroParams = new HashMap<String, String>();
        testSpace = new Space("tst");
        testPage = new Page();
        testPage.setTitle("Test Page");
        testPage.setSpace(testSpace);
    }

    @Override
    protected void tearDown() throws Exception
    {
        bookmarkUtils = null;
        spaceManager = null;
        pageManager = null;
        localeManager = null;
        i18NBeanFactory = null;
        i18NBean = null;
        webResourceManager = null;
        velocityHelperService = null;
        super.tearDown();
    }

    public void testBodyRenderMode()
    {
        assertTrue(RenderMode.NO_RENDER.equals(bookmarksMacro.getBodyRenderMode()));
    }

    public void testInline()
    {
        assertEquals(Macro.OutputType.BLOCK, bookmarksMacro.getOutputType());
    }

    public void testHasBody()
    {
        assertFalse(bookmarksMacro.hasBody());
    }

    public void testExecuteWithoutSpaceKeyReturnsEmptyContent()
    {
        testPage.setSpace(null);

        assertEquals(
                StringUtils.EMPTY,
                bookmarksMacro.execute(
                        macroParams,
                        StringUtils.EMPTY,
                        testPage.toPageContext()
                )
        );
        verify(velocityHelperService, never()).getRenderedTemplate(anyString(), Matchers.<Map<String, ?>>anyObject());
    }

    public void testExecuteWithSpaceKeyRendersBookmarks()
    {
        Bookmarks bookmarks = new Bookmarks();
        bookmarks.setBookmarkList(Collections.<Bookmark>emptyList());

        when(spaceManager.getSpace(anyString())).thenAnswer(
                new Answer<Space>()
                {
                    @Override
                    public Space answer(InvocationOnMock invocationOnMock) throws Throwable
                    {
                        return new Space(invocationOnMock.getArguments()[0].toString());
                    }
                }
        );

        when(bookmarkUtils.getBookmarkList(Matchers.<Set<String>>anyObject(), anyString(), anyString(), anyString(), anyInt(), anyInt(), anyBoolean())).thenReturn(bookmarks);

        String renderedContent = "renderedContent";
        when(velocityHelperService.getRenderedTemplate(
                eq("templates/plugins/socialbookmarking/bookmarklist.vm"),
                argThat(
                        new BaseMatcher<Map<String, ?>>()
                        {
                            public boolean matches(Object o)
                            {
                                @SuppressWarnings("unchecked")
                                Map<String, Object> velocityContext = (Map<String, Object>) o;
                                return testSpace.getKey().equals(velocityContext.get("spaces"));
                            }

                            public void describeTo(Description description)
                            {
                                description.appendText("Not a Velocity context containing the expected space keys");
                            }
                        }
                )
        )).thenReturn(renderedContent);

        macroParams.put(BookmarksMacro.SPACE_PARAM, testSpace.getKey());

        assertEquals(
                renderedContent,
                bookmarksMacro.execute(
                        macroParams,
                        StringUtils.EMPTY,
                        testPage.toPageContext()
                )
        );
    }

    public void testRenderUsingSpaceKeyFromRenderContext()
    {
        Bookmarks bookmarks = new Bookmarks();
        bookmarks.setBookmarkList(Collections.<Bookmark>emptyList());

        when(spaceManager.getSpace(anyString())).thenAnswer(
                new Answer<Space>()
                {
                    @Override
                    public Space answer(InvocationOnMock invocationOnMock) throws Throwable
                    {
                        return new Space(invocationOnMock.getArguments()[0].toString());
                    }
                }
        );
        
        when(bookmarkUtils.getBookmarkList(Matchers.<Set<String>>anyObject(), anyString(), anyString(), anyString(), anyInt(), anyInt(), anyBoolean())).thenReturn(bookmarks);

        String renderedContent = "renderedContent";
        when(velocityHelperService.getRenderedTemplate(
                eq("templates/plugins/socialbookmarking/bookmarklist.vm"),
                argThat(
                        new BaseMatcher<Map<String, ?>>()
                        {
                            public boolean matches(Object o)
                            {
                                @SuppressWarnings("unchecked")
                                Map<String, Object> velocityContext = (Map<String, Object>) o;
                                return testSpace.getKey().equals(velocityContext.get("spaces"));
                            }

                            public void describeTo(Description description)
                            {
                                description.appendText("Not a Velocity context containing the expected space keys");
                            }
                        }
                )
        )).thenReturn(renderedContent);

        assertEquals(
                renderedContent,
                bookmarksMacro.execute(
                        macroParams,
                        StringUtils.EMPTY,
                        testPage.toPageContext()
                )
        );
    }

    public void testExecuteInsideComment() throws MacroException
    {
        Comment testComment = new Comment();
        testComment.setOwner(testPage);

        when(spaceManager.getSpace(testPage.getSpaceKey())).thenReturn(testPage.getSpace());
        when(pageManager.getPage(testPage.getSpaceKey(), BookmarkUtils.BOOKMARK_PARENT_PAGE)).thenReturn(null);

        Bookmarks bookmarks = new Bookmarks();
        bookmarks.setBookmarkList(Collections.<Bookmark>emptyList());
        when(bookmarkUtils.getBookmarkList(Matchers.<Set<String>>anyObject(), anyString(), anyString(), anyString(), anyInt(), anyInt(), anyBoolean())).thenReturn(bookmarks);

        bookmarksMacro.execute(macroParams, StringUtils.EMPTY, testComment.toPageContext());

        verify(velocityHelperService).getRenderedTemplate(
                eq("templates/plugins/socialbookmarking/bookmarklist.vm"),
                argThat(
                        new BaseMatcher<Map<String, ?>>()
                        {
                            public boolean matches(Object o)
                            {
                                @SuppressWarnings("unchecked")
                                Map<String, Object> velocityContext = (Map<String, Object>) o;
                                return null != velocityContext.get("space")
                                        && StringUtils.equals(
                                        ((Space) velocityContext.get("space")).getKey(),
                                        testPage.getSpaceKey()
                                );
                            }

                            public void describeTo(Description description)
                            {
                                description.appendText("A Velocity context that does not contain the expected space");
                            }
                        }
                )
        );
    }

    public void testShowBookmarksFromMultipleSpaces()
    {
        final Bookmarks bookmarks = new Bookmarks();
        bookmarks.setBookmarkList(Collections.<Bookmark>emptyList());

        when(spaceManager.getSpace(anyString())).thenAnswer(
                new Answer<Space>()
                {
                    @Override
                    public Space answer(InvocationOnMock invocationOnMock) throws Throwable
                    {
                        Space s = new Space(invocationOnMock.getArguments()[0].toString());
                        s.setName(s.getKey());
                        return s;
                    }
                }
        );

        when(bookmarkUtils.getBookmarkList(eq(new LinkedHashSet<String>(Arrays.asList("ds", "ds2"))), anyString(), anyString(), anyString(), anyInt(), anyInt(), anyBoolean())).thenReturn(bookmarks);

        String renderedContent = "renderedContent";
        when(velocityHelperService.getRenderedTemplate(
                eq("templates/plugins/socialbookmarking/bookmarklist.vm"),
                argThat(
                        new BaseMatcher<Map<String, ?>>()
                        {
                            public boolean matches(Object o)
                            {
                                @SuppressWarnings("unchecked")
                                Map<String, Object> velocityContext = (Map<String, Object>) o;
                                return velocityContext.get("bookmarks") == bookmarks.getBookmarkList();
                            }

                            public void describeTo(Description description)
                            {
                                description.appendText("Not a Velocity context containing the expected space keys");
                            }
                        }
                )
        )).thenReturn(renderedContent);

        macroParams.put("spaces", "ds,ds2");
        assertEquals(renderedContent, bookmarksMacro.execute(macroParams, StringUtils.EMPTY, testPage.toPageContext()));
    }
}
