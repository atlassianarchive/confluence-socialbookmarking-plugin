package com.atlassian.confluence.plugins.socialbookmarking.macros;

import com.atlassian.confluence.core.ContentEntityObject;
import com.atlassian.confluence.languages.LocaleManager;
import com.atlassian.confluence.macro.Macro;
import com.atlassian.confluence.mail.Mail;
import com.atlassian.confluence.pages.Draft;
import com.atlassian.confluence.pages.Page;
import com.atlassian.confluence.pages.PageManager;
import com.atlassian.confluence.plugin.services.VelocityHelperService;
import com.atlassian.confluence.plugins.socialbookmarking.Bookmark;
import com.atlassian.confluence.plugins.socialbookmarking.BookmarkUtils;
import com.atlassian.confluence.renderer.PageContext;
import com.atlassian.confluence.security.PermissionManager;
import com.atlassian.confluence.setup.BootstrapManager;
import com.atlassian.confluence.spaces.Space;
import com.atlassian.confluence.spaces.SpaceManager;
import com.atlassian.confluence.util.i18n.I18NBean;
import com.atlassian.confluence.util.i18n.I18NBeanFactory;
import com.atlassian.plugin.webresource.WebResourceUrlProvider;
import com.atlassian.renderer.RenderContext;
import com.atlassian.renderer.links.LinkResolver;
import com.atlassian.renderer.v2.RenderMode;
import com.atlassian.renderer.v2.macro.MacroException;
import com.atlassian.user.User;
import junit.framework.TestCase;
import org.apache.commons.lang.StringUtils;
import org.apache.commons.lang.math.RandomUtils;
import org.hamcrest.BaseMatcher;
import org.hamcrest.Description;
import org.mockito.Matchers;
import org.mockito.Mock;
import org.mockito.MockitoAnnotations;
import org.mockito.invocation.InvocationOnMock;
import org.mockito.stubbing.Answer;

import java.util.Date;
import java.util.HashMap;
import java.util.Locale;
import java.util.Map;

import static org.mockito.Mockito.anyString;
import static org.mockito.Mockito.argThat;
import static org.mockito.Mockito.eq;
import static org.mockito.Mockito.verify;
import static org.mockito.Mockito.when;

public class TestBookmarkMacro extends TestCase
{
    @Mock
    private BookmarkUtils bookmarkUtils;

    @Mock
	private SpaceManager spaceManager;

    @Mock
	private LocaleManager localeManager;

    @Mock
	private I18NBeanFactory i18NBeanFactory;

    @Mock
    private I18NBean i18NBean;

    @Mock
	private PermissionManager permissionManager;

    @Mock
    private BootstrapManager bootstrapManager;

    @Mock
	private PageManager pageManager;

    @Mock
    private LinkResolver linkResolver;

    @Mock
    private WebResourceUrlProvider webResourceUrlProvider;

    @Mock
    private VelocityHelperService velocityHelperService;

    private BookmarkMacro bookmarkMacro;

    public void setUp() throws Exception
    {
        super.setUp();
        MockitoAnnotations.initMocks(this);

        when(bootstrapManager.getWebAppContextPath()).thenReturn("/");

        when(i18NBeanFactory.getI18NBean(Matchers.<Locale>anyObject())).thenReturn(i18NBean);
        when(i18NBean.getText(anyString())).thenAnswer(new Answer<String>()
        {
            public String answer(InvocationOnMock invocationOnMock) throws Throwable
            {
                return invocationOnMock.getArguments()[0].toString();
            }
        });

        bookmarkMacro = new BookmarkMacro(bookmarkUtils, spaceManager, localeManager, i18NBeanFactory, permissionManager, bootstrapManager, pageManager, linkResolver, webResourceUrlProvider, velocityHelperService);
    }

    @Override
    protected void tearDown() throws Exception
    {
        bookmarkUtils = null;
        spaceManager = null;
        localeManager = null;
        i18NBeanFactory = null;
        i18NBean = null;
        permissionManager = null;
        bootstrapManager = null;
        pageManager = null;
        linkResolver = null;
        velocityHelperService = null;
        super.tearDown();
    }

    public void testBodyRenderMode()
    {
        assertTrue(RenderMode.NO_RENDER.equals(bookmarkMacro.getBodyRenderMode()));
    }

    public void testInline()
    {
        assertEquals(Macro.OutputType.BLOCK, bookmarkMacro.getOutputType());
    }

    public void testHasBody()
    {
        assertTrue(bookmarkMacro.hasBody());
    }

    public void testExecuteWithoutRenderContext()
    {
        assertEquals("<div class=\"error\"><span class=\"error\">bookmark.error.unsupportedcontenttype</span> </div>", bookmarkMacro.execute(new HashMap<String, String>(), StringUtils.EMPTY, (RenderContext) null));
    }

    public void testExecuteWithMailInPageContext()
    {
        assertEquals("<div class=\"error\"><span class=\"error\">bookmark.error.unsupportedcontenttype</span> </div>", bookmarkMacro.execute(new HashMap<String, String>(), StringUtils.EMPTY, new PageContext(new Mail())));
    }

    public void testExecuteWithNoPageTitleAndSpaceKeyInPageParameterInMacroParameters() throws MacroException
    {
        final String macroBodyHtml = "macroBodyHtml";

        final Page page = new Page();
        page.setSpace(new Space("TST"));
        page.setCreationDate(new Date());


        when(bookmarkUtils.isSupportedContentType(Matchers.<ContentEntityObject>anyObject())).thenReturn(true);
        when(bookmarkUtils.convertPageToBookmark(Matchers.<ContentEntityObject>anyObject(), Matchers.<User>anyObject())).thenReturn(new Bookmark());
        when(bookmarkUtils.getSpaceFromContentEntity(Matchers.<ContentEntityObject>anyObject())).thenReturn(page.getSpace());
        when(bookmarkUtils.isValidProtocol(Matchers.<String>anyObject())).thenReturn(true);
        when(pageManager.getPage(Matchers.<String>anyObject(), Matchers.<String>anyObject())).thenReturn(new Page());

        bookmarkMacro.execute(
                new HashMap<String, String>(),
                macroBodyHtml,
                page.toPageContext());

        verify(velocityHelperService).getRenderedTemplate(
                eq("templates/plugins/socialbookmarking/viewbookmark.vm"),
                argThat(new BaseMatcher<Map<String, ?>>()
                {
                    public boolean matches(Object o)
                    {
                        @SuppressWarnings("unchecked")
                        Map<String, Object> velocityContext = (Map<String, Object>) o;
                        return page.getSpaceKey().equals(velocityContext.get("spaceKey"));
                    }

                    public void describeTo(Description description)
                    {
                        description.appendText("Not a Velocity context that contains the expected space key");
                    }
                })
        );
        verify(velocityHelperService).getRenderedTemplate(
                eq("templates/plugins/socialbookmarking/bookmark.vm"),
                argThat(new BaseMatcher<Map<String, ?>>()
                {
                    public boolean matches(Object o)
                    {
                        @SuppressWarnings("unchecked")
                        Map<String, Object> velocityContext = (Map<String, Object>) o;
                        return page.getSpaceKey().equals(velocityContext.get("spaceKey"))
                                && macroBodyHtml.equals(velocityContext.get("body"));
                    }

                    public void describeTo(Description description)
                    {
                        description.appendText("Not a Velocity context that contains the expected space key and macro body");
                    }
                })
        );
    }

    /*
     * <a href="http://developer.atlassian.com/jira/browse/MARK-42">MARK-42</a>.
     */
    public void testExecuteWithoutPageParameterAndContainingPageNotYetMarkedAsBookmark() throws MacroException
    {
        final String macroBodyHtml = "macroBodyHtml";

        final Page page = new Page();
        page.setSpace(new Space("TST"));
        page.setCreationDate(new Date());
        page.setId(RandomUtils.nextLong());
        
        when(bookmarkUtils.isSupportedContentType(Matchers.<ContentEntityObject>anyObject())).thenReturn(true);

        Bookmark bookmark = new Bookmark();
        bookmark.setId(page.getIdAsString());
        when(bookmarkUtils.convertPageToBookmark(Matchers.<ContentEntityObject>anyObject(), Matchers.<User>anyObject())).thenReturn(bookmark);
        when(bookmarkUtils.getSpaceFromContentEntity(Matchers.<ContentEntityObject>anyObject())).thenReturn(page.getSpace());
        when(bookmarkUtils.isValidProtocol(Matchers.<String>anyObject())).thenReturn(true);
        when(pageManager.getPage(Matchers.<String>anyObject(), Matchers.<String>anyObject())).thenReturn(new Page());

        bookmarkMacro.execute(
                new HashMap<String, String>(),
                macroBodyHtml,
                page.toPageContext());

        verify(velocityHelperService).getRenderedTemplate(
                eq("templates/plugins/socialbookmarking/viewbookmark.vm"),
                argThat(new BaseMatcher<Map<String, ?>>()
                {
                    public boolean matches(Object o)
                    {
                        @SuppressWarnings("unchecked")
                        Map<String, Object> velocityContext = (Map<String, Object>) o;
                        return page.getIdAsString().equals(((Bookmark) velocityContext.get("bookmark")).getId());
                    }

                    public void describeTo(Description description)
                    {
                        description.appendText("Not a Velocity context that contains the expected bookmark");
                    }
                })
        );
        verify(velocityHelperService).getRenderedTemplate(
                eq("templates/plugins/socialbookmarking/bookmark.vm"),
                argThat(new BaseMatcher<Map<String, ?>>()
                {
                    public boolean matches(Object o)
                    {
                        @SuppressWarnings("unchecked")
                        Map<String, Object> velocityContext = (Map<String, Object>) o;
                        return page.getIdAsString().equals(((Bookmark) velocityContext.get("bookmark")).getId());
                    }

                    public void describeTo(Description description)
                    {
                        description.appendText("Not a Velocity context that contains the expected bookmark");
                    }
                })
        );
    }

    public void testExecuteWithDraft() throws MacroException
    {
        
        final Draft draft = new Draft();
        draft.setDraftSpaceKey("TST");
        draft.setCreationDate(new Date());

        Space draftSpace = new Space(draft.getDraftSpaceKey());
        when(spaceManager.getSpace(anyString())).thenReturn(draftSpace);
        when(bookmarkUtils.isSupportedContentType(Matchers.<ContentEntityObject>anyObject())).thenReturn(true);
        when(bookmarkUtils.convertPageToBookmark(Matchers.<ContentEntityObject>anyObject(), Matchers.<User>anyObject())).thenReturn(new Bookmark());
        when(bookmarkUtils.getSpaceFromContentEntity(Matchers.<ContentEntityObject>anyObject())).thenReturn(draftSpace);
        when(bookmarkUtils.isValidProtocol(Matchers.<String>anyObject())).thenReturn(true);
        when(pageManager.getPage(Matchers.<String>anyObject(), Matchers.<String>anyObject())).thenReturn(new Page());

        bookmarkMacro.execute(new HashMap<String, String>(), "", draft.toPageContext());

        verify(velocityHelperService).getRenderedTemplate(
                eq("templates/plugins/socialbookmarking/viewbookmark.vm"),
                argThat(new BaseMatcher<Map<String, ?>>()
                {
                    public boolean matches(Object o)
                    {
                        @SuppressWarnings("unchecked")
                        Map<String, Object> velocityContext = (Map<String, Object>) o;
                        return draft.getDraftSpaceKey().equals(velocityContext.get("spaceKey"));
                    }

                    public void describeTo(Description description)
                    {
                        description.appendText("Not a Velocity context that contains the expected space key");
                    }
                })
        );
        verify(velocityHelperService).getRenderedTemplate(
                eq("templates/plugins/socialbookmarking/bookmark.vm"),
                argThat(new BaseMatcher<Map<String, ?>>()
                {
                    public boolean matches(Object o)
                    {
                        @SuppressWarnings("unchecked")
                        Map<String, Object> velocityContext = (Map<String, Object>) o;
                        return draft.getDraftSpaceKey().equals(velocityContext.get("spaceKey"));
                    }

                    public void describeTo(Description description)
                    {
                        description.appendText("Not a Velocity context that contains the expected space key");
                    }
                })
        );
    }

	public void testEditAndRemoveBookmarkLinksNotAllowedInPreview()
	{

        final String macroBodyHtml = "macroBodyHtml";

        final Page page = new Page();
        page.setSpace(new Space("TST"));
        page.setCreationDate(new Date());
        page.setId(RandomUtils.nextLong());
        
        when(bookmarkUtils.isSupportedContentType(Matchers.<ContentEntityObject>anyObject())).thenReturn(true);
        when(bookmarkUtils.isValidProtocol(Matchers.<String>anyObject())).thenReturn(true);
        when(pageManager.getPage(Matchers.<String>anyObject(), Matchers.<String>anyObject())).thenReturn(new Page());

        Bookmark bookmark = new Bookmark();
        bookmark.setId(page.getIdAsString());
        when(bookmarkUtils.convertPageToBookmark(Matchers.<ContentEntityObject>anyObject(), Matchers.<User>anyObject())).thenReturn(bookmark);

        when(bookmarkUtils.getSpaceFromContentEntity(Matchers.<ContentEntityObject>anyObject())).thenReturn(page.getSpace());

        RenderContext renderContext = page.toPageContext();
        renderContext.setOutputType(RenderContext.PREVIEW);
        bookmarkMacro.execute(
                new HashMap<String, String>(),
                macroBodyHtml,
                renderContext);

        verify(velocityHelperService).getRenderedTemplate(
                eq("templates/plugins/socialbookmarking/viewbookmark.vm"),
                argThat(new BaseMatcher<Map<String, ?>>()
                {
                    public boolean matches(Object o)
                    {
                        @SuppressWarnings("unchecked")
                        Map<String, Object> velocityContext = (Map<String, Object>) o;
                        Bookmark bookmark = (Bookmark) velocityContext.get("bookmark");
                        return StringUtils.equals(page.getIdAsString(), bookmark.getId())
                                && !bookmark.isEditPermission()
                                && !bookmark.isRemovePermission();
                    }

                    public void describeTo(Description description)
                    {
                        description.appendText("Not a Velocity context that contains the bookmark");
                    }
                })
        );
        verify(velocityHelperService).getRenderedTemplate(
                eq("templates/plugins/socialbookmarking/bookmark.vm"),
                argThat(new BaseMatcher<Map<String, ?>>()
                {
                    public boolean matches(Object o)
                    {
                        @SuppressWarnings("unchecked")
                        Map<String, Object> velocityContext = (Map<String, Object>) o;
                        Bookmark bookmark = (Bookmark) velocityContext.get("bookmark");
                        return StringUtils.equals(page.getIdAsString(), bookmark.getId())
                                && !bookmark.isEditPermission()
                                && !bookmark.isRemovePermission();
                    }

                    public void describeTo(Description description)
                    {
                        description.appendText("Not a Velocity context that contains the bookmark");
                    }
                })
        );
	}
}
