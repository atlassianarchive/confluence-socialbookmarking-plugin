package com.atlassian.confluence.plugins.socialbookmarking.actions;

import bucket.core.actions.PaginationSupport;
import com.atlassian.confluence.languages.LocaleManager;
import com.atlassian.confluence.pages.Page;
import com.atlassian.confluence.plugins.socialbookmarking.Bookmark;
import com.atlassian.confluence.plugins.socialbookmarking.BookmarkUtils;
import com.atlassian.confluence.plugins.socialbookmarking.Bookmarks;
import com.atlassian.confluence.spaces.Space;
import com.atlassian.confluence.themes.ThemeHelper;
import com.atlassian.confluence.util.i18n.I18NBean;
import com.atlassian.confluence.util.i18n.I18NBeanFactory;
import com.atlassian.xwork.results.RssResult;
import com.opensymphony.xwork.ActionSupport;
import com.sun.syndication.feed.synd.SyndFeedImpl;
import junit.framework.TestCase;
import org.mockito.Matchers;
import org.mockito.Mock;
import org.mockito.MockitoAnnotations;
import org.mockito.invocation.InvocationOnMock;
import org.mockito.stubbing.Answer;

import java.util.Arrays;
import java.util.Collections;
import java.util.List;
import java.util.Locale;
import java.util.Map;
import java.util.Set;

import static org.mockito.Mockito.*;

public class TestBookmarksAction extends TestCase
{
    @Mock
    private LocaleManager localeManager;

    @Mock
    private I18NBeanFactory i18NBeanFactory;

    @Mock
    private I18NBean i18NBean;

    @Mock
    private BookmarkUtils bookmarkUtils;

    private BookmarksAction bookmarksAction;

    private Space space;

    public void setUp() throws Exception
    {
        super.setUp();
        MockitoAnnotations.initMocks(this);

        when(i18NBeanFactory.getI18NBean(Matchers.<Locale>anyObject())).thenReturn(i18NBean);
        when(i18NBean.getText(anyString(), Matchers.<Object[]>anyObject())).thenAnswer(
                new Answer<String>()
                {
                    public String answer(InvocationOnMock invocationOnMock) throws Throwable
                    {
                        return invocationOnMock.getArguments()[0].toString();
                    }
                }
        );


        bookmarksAction = new BookmarksAction();
        bookmarksAction.setI18NBeanFactory(i18NBeanFactory);
        bookmarksAction.setLocaleManager(localeManager);
        bookmarksAction.setBookmarkUtils(bookmarkUtils);
    }

    @Override
    protected void tearDown() throws Exception
    {
        localeManager = null;
        i18NBeanFactory = null;
        i18NBean = null;
        bookmarkUtils = null;
        super.tearDown();
    }

    public void testExecuteWithOnlyPage() throws Exception
    {
        final PaginationSupport paginationSupport;

        bookmarksAction.setPage(new Page());
        bookmarksAction.execute();

        /* Getting bookmarks via page ID will return only 1 item */
        assertEquals(1, bookmarksAction.getBookmarks().size());

        paginationSupport = bookmarksAction.getPaginationSupport();
        assertEquals(1, paginationSupport.getTotal());
    }

    public void testExecuteWithPageAndRssOutputType() throws Exception
    {
        final String spaceKey = "TST";

        space = new Space(spaceKey);
        bookmarksAction.setSpace(space);
        bookmarksAction.setPage(new Page());
        bookmarksAction.setOutput(RssResult.RSS);

        assertEquals(RssResult.RSS, bookmarksAction.execute());
    }

    public void testExecuteWithPageAndWikiOutputType() throws Exception
    {
        bookmarksAction.setPage(new Page());
        bookmarksAction.setOutput(ActionSupport.SUCCESS);
        assertTrue(bookmarksAction.execute().equals(ActionSupport.SUCCESS));
        assertFalse(bookmarksAction.execute().equals(RssResult.RSS));
    }

    public void testExecuteWithoutPage() throws Exception
    {
        Bookmarks bookmarks = new Bookmarks();
        bookmarks.setBookmarkList(Arrays.asList(new Bookmark(), new Bookmark()));
        when(bookmarkUtils.getBookmarkList(Matchers.<Set<String>>anyObject(), anyString(), anyString(), anyString(), anyInt(), anyInt(), anyBoolean())).thenReturn(bookmarks);

        bookmarksAction.setSpace(new Space("TST"));
        bookmarksAction.execute();

        /* Getting bookmarks via page ID will return only 1 item */
        assertEquals(2, bookmarksAction.getBookmarks().size());

        PaginationSupport paginationSupport = bookmarksAction.getPaginationSupport();
        assertEquals(2, paginationSupport.getTotal());
    }

    public void testExecuteWithoutPageAndRssOutputType() throws Exception
    {
        Bookmarks bookmarks = new Bookmarks();
        bookmarks.setBookmarkList(Collections.<Bookmark>emptyList());
        when(bookmarkUtils.getBookmarkList(Matchers.<Set<String>>anyObject(), anyString(), anyString(), anyString(), anyInt(), anyInt(), anyBoolean())).thenReturn(bookmarks);

        space = new Space("TST");
        bookmarksAction.setSpace(space);
        bookmarksAction.setOutput(RssResult.RSS);

        assertEquals(RssResult.RSS, bookmarksAction.execute());
    }

    public void testExecuteWithoutPageAndWikiOutputType() throws Exception
    {
        Bookmarks bookmarks = new Bookmarks();
        bookmarks.setBookmarkList(Collections.<Bookmark>emptyList());
        when(bookmarkUtils.getBookmarkList(Matchers.<Set<String>>anyObject(), anyString(), anyString(), anyString(), anyInt(), anyInt(), anyBoolean())).thenReturn(bookmarks);

        bookmarksAction.setSpace(new Space("TST"));
        bookmarksAction.setOutput(ActionSupport.SUCCESS);
        assertTrue(bookmarksAction.execute().equals(ActionSupport.SUCCESS));
        assertFalse(bookmarksAction.execute().equals(RssResult.RSS));
    }

    /*
     * MARK-3
     */
    public void testGetRssFeedInSpaceDescriptionWhenMaxBookmarksIsNotSpecified() throws Exception
    {
        Bookmarks bookmarks = new Bookmarks();
        bookmarks.setBookmarkList(Collections.<Bookmark>emptyList());
        when(bookmarkUtils.getBookmarkList(Matchers.<Set<String>>anyObject(), anyString(), anyString(), anyString(), anyInt(), anyInt(), anyBoolean())).thenReturn(bookmarks);

        when(bookmarkUtils.createSyndFeed(Matchers.<List<Bookmark>>anyObject(), Matchers.<ThemeHelper>anyObject(), Matchers.<Map<String,String>>anyObject())).thenReturn(new SyndFeedImpl());

        space = new Space("TST");
        bookmarksAction.setSpace(space);
        bookmarksAction.setOutput(RssResult.RSS);

        assertEquals(RssResult.RSS, bookmarksAction.execute());
        assertEquals("bookmarks.inspace", bookmarksAction.getSyndFeed().getDescription()); /* Should be equals to the i18n key */
    }

    /*
     * MARK-3
     */
    public void testGetRssFeedInSpaceDescriptionWhenLabelsNotSpecified() throws Exception
    {
        Bookmarks bookmarks = new Bookmarks();
        bookmarks.setBookmarkList(Collections.<Bookmark>emptyList());
        when(bookmarkUtils.getBookmarkList(Matchers.<Set<String>>anyObject(), anyString(), anyString(), anyString(), anyInt(), anyInt(), anyBoolean())).thenReturn(bookmarks);

        when(bookmarkUtils.createSyndFeed(Matchers.<List<Bookmark>>anyObject(), Matchers.<ThemeHelper>anyObject(), Matchers.<Map<String,String>>anyObject())).thenReturn(new SyndFeedImpl());

        space = new Space("TST");
        bookmarksAction.setSpace(space);
        bookmarksAction.setOutput(RssResult.RSS);
        bookmarksAction.setMax(String.valueOf(1));

        assertEquals(RssResult.RSS, bookmarksAction.execute());
        assertEquals("bookmarks.inspace.withcount", bookmarksAction.getSyndFeed().getDescription()); /* Should be equals to the i18n key */
    }

    /*
     * MARK-3
     */
    public void testGetRssFeedInSpaceDescriptionWhenLabelsSpecified() throws Exception
    {
        Bookmarks bookmarks = new Bookmarks();
        bookmarks.setBookmarkList(Collections.<Bookmark>emptyList());
        when(bookmarkUtils.getBookmarkList(Matchers.<Set<String>>anyObject(), anyString(), anyString(), anyString(), anyInt(), anyInt(), anyBoolean())).thenReturn(bookmarks);

        when(bookmarkUtils.createSyndFeed(Matchers.<List<Bookmark>>anyObject(), Matchers.<ThemeHelper>anyObject(), Matchers.<Map<String,String>>anyObject())).thenReturn(new SyndFeedImpl());

        space = new Space("TST");
        bookmarksAction.setSpace(space);
        bookmarksAction.setOutput(RssResult.RSS);
        bookmarksAction.setMax(String.valueOf(1));
        bookmarksAction.setLabels("foo,bar");

        assertEquals(RssResult.RSS, bookmarksAction.execute());
        assertEquals("bookmarks.inspace.withcountandlabels", bookmarksAction.getSyndFeed().getDescription()); /* Should be equals to the i18n key */
    }

    /*
     * MARK-3
     */
    public void testGetRssFeedForSpaceDescriptionWhenMaxBookmarkIsNotSpecified() throws Exception
    {
        Bookmarks bookmarks = new Bookmarks();
        bookmarks.setBookmarkList(Collections.<Bookmark>emptyList());
        when(bookmarkUtils.getBookmarkList(Matchers.<Set<String>>anyObject(), anyString(), anyString(), anyString(), anyInt(), anyInt(), anyBoolean())).thenReturn(bookmarks);

        when(bookmarkUtils.createSyndFeed(Matchers.<List<Bookmark>>anyObject(), Matchers.<ThemeHelper>anyObject(), Matchers.<Map<String,String>>anyObject())).thenReturn(new SyndFeedImpl());

        bookmarksAction.setSpace(new Space("TST"));
        bookmarksAction.setOutput(RssResult.RSS);
        bookmarksAction.setLabels("foo,bar");
        bookmarksAction.setMode(BookmarksAction.MODE_BOOKMARKSFORSPACE);

        assertEquals(RssResult.RSS, bookmarksAction.execute());
        assertEquals("bookmarks.forspace", bookmarksAction.getSyndFeed().getDescription()); /* Should be equals to the i18n key */
    }

    /*
     * MARK-3
     */
    public void testGetRssFeedForSpaceDescriptionWhenMaxBookmarkIsSpecified() throws Exception
    {
        Bookmarks bookmarks = new Bookmarks();
        bookmarks.setBookmarkList(Collections.<Bookmark>emptyList());
        when(bookmarkUtils.getBookmarkList(Matchers.<Set<String>>anyObject(), anyString(), anyString(), anyString(), anyInt(), anyInt(), anyBoolean())).thenReturn(bookmarks);

        when(bookmarkUtils.createSyndFeed(Matchers.<List<Bookmark>>anyObject(), Matchers.<ThemeHelper>anyObject(), Matchers.<Map<String,String>>anyObject())).thenReturn(new SyndFeedImpl());

        bookmarksAction.setSpace(new Space("TST"));
        bookmarksAction.setOutput(RssResult.RSS);
        bookmarksAction.setMode(BookmarksAction.MODE_BOOKMARKSFORSPACE);
        bookmarksAction.setMax(String.valueOf(1));
        bookmarksAction.setLabels("foo,bar");

        assertEquals(RssResult.RSS, bookmarksAction.execute());
        assertEquals("bookmarks.forspace.withcount", bookmarksAction.getSyndFeed().getDescription()); /* Should be equals to the i18n key */
    }
}
