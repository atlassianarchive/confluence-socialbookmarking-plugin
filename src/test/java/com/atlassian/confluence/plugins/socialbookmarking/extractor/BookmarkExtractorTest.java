package com.atlassian.confluence.plugins.socialbookmarking.extractor;

import com.atlassian.confluence.core.ContentPropertyManager;
import com.atlassian.confluence.pages.AbstractPage;
import com.atlassian.confluence.pages.Comment;
import com.atlassian.confluence.pages.Page;
import com.atlassian.confluence.plugins.socialbookmarking.BookmarkUtils;
import junit.framework.TestCase;
import org.apache.lucene.document.Document;
import org.apache.lucene.index.IndexableField;
import org.mockito.Mock;
import org.mockito.MockitoAnnotations;

import static org.mockito.Mockito.when;

public class BookmarkExtractorTest extends TestCase
{

    @Mock
    private ContentPropertyManager contentPropertyManager;

    private BookmarkExtractor bookmarkExtractor;

    private AbstractPage bookmarkPage;

    protected void setUp() throws Exception
    {
        super.setUp();
        MockitoAnnotations.initMocks(this);

        bookmarkPage = new Page();

        bookmarkExtractor = new BookmarkExtractor();
        bookmarkExtractor.setContentPropertyManager(contentPropertyManager);
    }

    public void testIfBookmarkFieldIsAddedWhenPageIsBookmark()
    {
        final Document documentToBeIndexed;
        final IndexableField bookmarkField;

        when(contentPropertyManager.getStringProperty(bookmarkPage, BookmarkUtils.ISBOOKMARK_PROPERTY_KEY)).thenReturn("true");

        documentToBeIndexed = new Document();
        bookmarkExtractor.addFields(documentToBeIndexed, new StringBuffer(), bookmarkPage);
        bookmarkField = documentToBeIndexed.getField("bookmark");

        assertNotNull(bookmarkField);
        assertTrue(bookmarkField.fieldType().tokenized());
        assertEquals("yes true", bookmarkField.stringValue());
    }

    public void testIfBookmarkFieldIsAddedWhenPageIsNotBookmark()
    {
        final Document documentToBeIndexed;

        when(contentPropertyManager.getStringProperty(bookmarkPage, BookmarkUtils.ISBOOKMARK_PROPERTY_KEY)).thenReturn("false");

        documentToBeIndexed = new Document();

        bookmarkExtractor.addFields(documentToBeIndexed, new StringBuffer(), bookmarkPage);

        assertNull(documentToBeIndexed.getField("bookmark"));
    }

    public void testIfBookmarkFieldIsAddedWhenDocumentBeingIndexedIsNotOfTypeAbstractPage()
    {
        final Document documentToBeIndexed;
        final IndexableField bookmarkField;

        documentToBeIndexed = new Document();
        bookmarkExtractor.addFields(documentToBeIndexed, new StringBuffer(), new Comment());
        bookmarkField = documentToBeIndexed.getField("bookmark");

        assertNull(bookmarkField);
    }
}
