package com.atlassian.confluence.plugins.socialbookmarking.actions;

import com.atlassian.confluence.core.ContentEntityObject;
import com.atlassian.confluence.core.SaveContext;
import com.atlassian.confluence.labels.LabelManager;
import com.atlassian.confluence.pages.Page;
import com.atlassian.confluence.pages.PageManager;
import com.atlassian.confluence.plugins.socialbookmarking.Bookmark;
import com.atlassian.confluence.plugins.socialbookmarking.BookmarkUtils;
import com.atlassian.confluence.setup.BootstrapManager;
import com.atlassian.confluence.spaces.Space;
import com.atlassian.confluence.spaces.SpaceManager;
import com.atlassian.confluence.user.AuthenticatedUserThreadLocal;
import com.atlassian.confluence.user.ConfluenceUser;
import com.atlassian.confluence.user.ConfluenceUserImpl;
import com.atlassian.confluence.xhtml.api.XhtmlContent;
import com.atlassian.user.User;
import com.atlassian.user.impl.DefaultUser;
import com.opensymphony.xwork.ActionSupport;
import junit.framework.TestCase;
import org.mockito.ArgumentMatcher;
import org.mockito.Matchers;
import org.mockito.Mock;
import org.mockito.MockitoAnnotations;

import java.util.ArrayList;
import java.util.List;

import static com.atlassian.confluence.plugins.socialbookmarking.actions.UpdateBookmarkAction.URL_INVALID;
import static com.atlassian.confluence.plugins.socialbookmarking.actions.UpdateBookmarkAction.URL_REQUIRED;
import static org.apache.commons.lang.StringUtils.EMPTY;
import static org.mockito.Mockito.anyBoolean;
import static org.mockito.Mockito.argThat;
import static org.mockito.Mockito.same;
import static org.mockito.Mockito.verify;
import static org.mockito.Mockito.when;

public class TestUpdateBookmarkAction extends TestCase
{
    private UpdateBookmarkAction updateBookmarkAction;
    private ConfluenceUser user;

    @Mock
    private BootstrapManager bootstrapManager;
    
    @Mock
    private SpaceManager spaceManager;

    @Mock
    private BookmarkUtils bookmarkUtils;

    @Mock
    private PageManager pageManager;

    @Mock
    private LabelManager labelManager;

    @Mock
    private XhtmlContent xhtmlContent;

    @SuppressWarnings("serial")
	protected void setUp() throws Exception
    {
        super.setUp();

        MockitoAnnotations.initMocks(this);

        when(bootstrapManager.getWebAppContextPath()).thenReturn("");

        updateBookmarkAction = new UpdateBookmarkAction()
        {
        	public String getText(String key)
        	{
        		return key;
        	}

            @Override
            void setBookmarkPageContent(Page bookmarkPage, String bookmarkPageContent)
            {
                // Does nothing
            }
        };
        updateBookmarkAction.setBootstrapManager(bootstrapManager);
        updateBookmarkAction.setSpaceManager(spaceManager);
        updateBookmarkAction.setBookmarkUtils(bookmarkUtils);
        updateBookmarkAction .setPageManager(pageManager);
        updateBookmarkAction.setLabelManager(labelManager);
        
        user = new ConfluenceUserImpl(new DefaultUser("testuser"));
        AuthenticatedUserThreadLocal.set(user);
    }

    @Override
    protected void tearDown()
    {
        AuthenticatedUserThreadLocal.set(null);
    }

    public void testRedirectOnBookmarkCreationCancellationFromBookmarklet() throws Exception
    {
        /* MARK-56 */
        updateBookmarkAction.setUrl("http://www.atlassian.jp/"); /* http://www.atlassian.jp/ URL encoded */
        updateBookmarkAction.setCancel("cancel");

        assertEquals("bookmarkurl", updateBookmarkAction.getCancelResult());
        assertEquals("http://www.atlassian.jp/", updateBookmarkAction.getBookmarkRedirectUrl());
    }

    public void testGetPersonalSpaceIfSpaceKeyIsNull() throws Exception
    {
        final Space personalSpace = new Space("~" + user.getName());
        Page bookmarkParent = new Page()
        {
            @Override
            public List<Page> getChildren()
            {
                return new ArrayList<Page>();
            }
        };

        when(spaceManager.getPersonalSpace(user)).thenReturn(personalSpace);
        when(bookmarkUtils.getBookmarkParent(same(personalSpace), anyBoolean())).thenReturn(bookmarkParent);

        updateBookmarkAction.execute();
        verify(pageManager).saveContentEntity(
                argThat(
                        new ArgumentMatcher<ContentEntityObject>()
                        {
                            @Override
                            public boolean matches(Object o)
                            {
                                Page bookmarkPage = (Page) o;
                                return bookmarkPage.getSpaceKey().equals(personalSpace.getKey());
                            }
                        }
                ),
                Matchers.<SaveContext>anyObject()
        );
    }
    
    public void testUserPersonalSpaceSelectedByDefaultWhenCreatingBookmarkViaBookmarklet() throws Exception
    {
        String spaceKey = "~admin";
        when(spaceManager.getPersonalSpace(Matchers.<User>anyObject())).thenReturn(new Space(spaceKey));

        assertEquals(ActionSupport.INPUT, updateBookmarkAction.doDefault());
        assertEquals(spaceKey, updateBookmarkAction.getSpaceKey());
    }

    public void testBookmarkSpaceSelectedByDefaultWhenEditingBookmark() throws Exception
    {
        Page bookmarkPage = new Page();
        bookmarkPage.setId(1);
        bookmarkPage.setSpace(new Space("TST"));
        when(pageManager.getPage(bookmarkPage.getId())).thenReturn(bookmarkPage);

        Bookmark bookmark = new Bookmark();
        when(bookmarkUtils.convertPageToBookmark(Matchers.<ContentEntityObject>anyObject(), Matchers.<User>anyObject())).thenReturn(bookmark);

        updateBookmarkAction.setBookmarkPageId(bookmarkPage.getIdAsString());

        assertEquals(ActionSupport.INPUT, updateBookmarkAction.doDefault());
        assertEquals(bookmarkPage.getSpaceKey(), updateBookmarkAction.getSpaceKey());
    }

    public void testValidateEmptyUrl()
    {
        updateBookmarkAction.validateUrl(EMPTY);
        assertTrue(updateBookmarkAction.getActionErrors().contains(URL_REQUIRED));
    }

    public void testValidateXss()
    {
        updateBookmarkAction.validateUrl("javascript:alert(document.cookie);");
        assertTrue(updateBookmarkAction.getActionErrors().contains(URL_INVALID));
    }

    public void testValidateEncodedXss()
    {
        updateBookmarkAction.validateUrl("data:text/html;base64,PHNjcmlwdD5hbGVydChkb2N1bWVudC5jb29raWUpPC9zY3JpcHQ+DQo=");
        assertTrue(updateBookmarkAction.getActionErrors().contains(URL_INVALID));
    }

}
