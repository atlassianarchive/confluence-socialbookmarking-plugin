package com.atlassian.confluence.plugins.socialbookmarking;

import junit.framework.TestCase;

public class TestBookmarkComparator extends TestCase
{
    public void testCompareBookmarkWithoutNextComparator()
    {
        assertEquals(Integer.MAX_VALUE, new AbstractBookmarkComparator()
        {
            @Override
            protected int compareInternal(Bookmark leftBookmark, Bookmark rightBookmark)
            {
                return Integer.MAX_VALUE;
            }
        }.compare(null, null));
    }

    public void testCompareBookmarkWithNextComparator()
    {
        assertEquals(Integer.MIN_VALUE, new AbstractBookmarkComparator()
        {
            @Override
            protected int compareInternal(Bookmark leftBookmark, Bookmark rightBookmark)
            {
                return 0;
            }

            @Override
            public AbstractBookmarkComparator getNextComparator()
            {
                return new AbstractBookmarkComparator()
                {
                    @Override
                    protected int compareInternal(Bookmark leftBookmark, Bookmark rightBookmark)
                    {
                        return Integer.MIN_VALUE;
                    }
                };
            }
        }.compare(null, null));
    }
}
