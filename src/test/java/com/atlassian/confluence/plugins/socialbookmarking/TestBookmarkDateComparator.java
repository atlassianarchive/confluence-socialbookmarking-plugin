package com.atlassian.confluence.plugins.socialbookmarking;


import junit.framework.TestCase;

import java.util.Arrays;
import java.util.Collections;
import java.util.Date;
import java.util.List;

public class TestBookmarkDateComparator extends TestCase
{
    private BookmarkDateComparator bookmarkDateComparator;

    @Override
    protected void setUp() throws Exception
    {
        super.setUp();
        bookmarkDateComparator = new BookmarkDateComparator();
    }

    public void testSortInDescendingOrder()
    {
        Bookmark one = new Bookmark();
        one.setAddedDate(new Date(System.currentTimeMillis()));

        Bookmark two = new Bookmark();
        two.setAddedDate(new Date(one.getAddedDate().getTime() + 1));

        List<Bookmark> bookmarksToBeSorted = Arrays.asList(one, two);
        Collections.sort(bookmarksToBeSorted, bookmarkDateComparator);

        assertEquals(
                Arrays.asList(two, one),
                bookmarksToBeSorted
        );
    }
}
