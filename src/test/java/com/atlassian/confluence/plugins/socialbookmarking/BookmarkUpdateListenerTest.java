package com.atlassian.confluence.plugins.socialbookmarking;

import com.atlassian.confluence.core.ContentEntityObject;
import com.atlassian.confluence.core.ContentPropertyManager;
import com.atlassian.confluence.event.events.content.page.PageCreateEvent;
import com.atlassian.confluence.event.events.content.page.PageUpdateEvent;
import com.atlassian.confluence.pages.Page;
import junit.framework.TestCase;
import org.mockito.Mock;
import org.mockito.MockitoAnnotations;

import java.util.HashMap;

import static org.mockito.Mockito.anyObject;
import static org.mockito.Mockito.anyString;
import static org.mockito.Mockito.never;
import static org.mockito.Mockito.verify;
import static org.mockito.Mockito.when;

public class BookmarkUpdateListenerTest extends TestCase
{
    @Mock
    private ContentPropertyManager contentPropertyManager;

    @Mock
    private BookmarkMacroParser bookmarkMacroParser;

    private Page testPage;

    private Page testPageParent;

    private BookmarkUpdateListener bookmarkUpdateListener;

    private String url = "http://www.atlassian.com/";

    protected void setUp() throws Exception
    {
        super.setUp();
        MockitoAnnotations.initMocks(this);
        
        testPageParent = new Page();
        testPageParent.setTitle(BookmarkUtils.BOOKMARK_PARENT_PAGE);
        testPage = new Page();
        testPage.setParentPage(testPageParent);

        bookmarkUpdateListener = new BookmarkUpdateListener();
        bookmarkUpdateListener.setBookmarkMacroParser(bookmarkMacroParser);
        bookmarkUpdateListener.setContentPropertyManager(contentPropertyManager);
    }

    @Override
    protected void tearDown() throws Exception
    {
        contentPropertyManager = null;
        bookmarkMacroParser = null;
        super.tearDown();
    }

    public void testHandleBookmarkUpdateEventWithValidParentPageAndHasBookmarkMacroInPageContent()
    {
        when(bookmarkMacroParser.getParameters(testPage)).thenReturn(
                new HashMap<String, String>()
                {
                    {
                        put("url", url);
                    }
                }
        );

        bookmarkUpdateListener.handleEvent(new PageCreateEvent(testPage, testPage));

        /* If the ContentPropertyManager is called with the expected values, we're good */
        verify(contentPropertyManager).setTextProperty(testPage, BookmarkUtils.URL_PROPERTY_KEY, url);
        verify(contentPropertyManager).setStringProperty(testPage, BookmarkUtils.ISBOOKMARK_PROPERTY_KEY, "true");
    }

    public void testHandleBookmarkUpdateEventWithValidParentPageButNoBookmarkMacroInPageContent()
    {
        when(bookmarkMacroParser.getParameters(testPage)).thenReturn(new HashMap<String, String>());
        
        bookmarkUpdateListener.handleEvent(new PageUpdateEvent(testPage, testPage));

        verify(contentPropertyManager).setTextProperty(testPage, BookmarkUtils.URL_PROPERTY_KEY, null);
        verify(contentPropertyManager).setStringProperty(testPage, BookmarkUtils.ISBOOKMARK_PROPERTY_KEY, null);
    }

    public void testHandleBookmarkUpdateEventWithNoParentPage()
    {
        when(bookmarkMacroParser.getParameters(testPage)).thenReturn(new HashMap<String, String>());
        testPage.setParentPage(null);

        bookmarkUpdateListener.handleEvent(new PageUpdateEvent(testPage, testPage));

        verify(contentPropertyManager).setTextProperty(testPage, BookmarkUtils.URL_PROPERTY_KEY, null);
        verify(contentPropertyManager).setStringProperty(testPage, BookmarkUtils.ISBOOKMARK_PROPERTY_KEY, null);
    }

    public void testHandleBookmarkUpdateEventWithInvalidParentPageTitle()
    {
        when(bookmarkMacroParser.getParameters(testPage)).thenReturn(
                new HashMap<String, String>()
                {
                    {
                        put("url", url);
                    }
                }
        );
        
        testPageParent.setTitle("Not a .bookmark");

        bookmarkUpdateListener.handleEvent(new PageUpdateEvent(testPage, testPage));

        verify(contentPropertyManager).setTextProperty(testPage, BookmarkUtils.URL_PROPERTY_KEY, url);
        verify(contentPropertyManager).setStringProperty(testPage, BookmarkUtils.ISBOOKMARK_PROPERTY_KEY, "true");
    }

    public void testHandleBookmarkCreateEventWithValidParentPageAndHasBookmarkMacroInPageContent()
    {
        when(bookmarkMacroParser.getParameters(testPage)).thenReturn(
                new HashMap<String, String>()
                {
                    {
                        put("url", url);
                    }
                }
        );

        bookmarkUpdateListener.handleEvent(new PageCreateEvent(testPage, testPage));

        /* If the ContentPropertyManager is called with the expected values, we're good */
        verify(contentPropertyManager).setTextProperty(testPage, BookmarkUtils.URL_PROPERTY_KEY, url);
        verify(contentPropertyManager).setStringProperty(testPage, BookmarkUtils.ISBOOKMARK_PROPERTY_KEY, "true");
    }

    public void testHandleBookmarkCreateEventWithValidParentPageButNoBookmarkMacroInPageContent()
    {
        when(bookmarkMacroParser.getParameters(testPage)).thenReturn(new HashMap<String, String>());
        bookmarkUpdateListener.handleEvent(new PageCreateEvent(testPage, testPage));

        verify(contentPropertyManager, never()).setTextProperty((ContentEntityObject) anyObject(), anyString(), anyString());
        verify(contentPropertyManager, never()).setStringProperty((ContentEntityObject) anyObject(), anyString(), anyString());
    }

    public void testHandleBookmarkCreateEventWithNoParentPage()
    {
        when(bookmarkMacroParser.getParameters(testPage)).thenReturn(new HashMap<String, String>());
        testPage.setParentPage(null);

        bookmarkUpdateListener.handleEvent(new PageCreateEvent(testPage, testPage));

        verify(contentPropertyManager, never()).setTextProperty((ContentEntityObject) anyObject(), anyString(), anyString());
        verify(contentPropertyManager, never()).setStringProperty((ContentEntityObject) anyObject(), anyString(), anyString());
    }

    public void testHandleBookmarkCreateEventWithInvalidParentPageTitle()
    {
        when(bookmarkMacroParser.getParameters(testPage)).thenReturn(new HashMap<String, String>());
        testPageParent.setTitle("Not a .bookmark");

        bookmarkUpdateListener.handleEvent(new PageCreateEvent(testPage, testPage));

        verify(contentPropertyManager, never()).setTextProperty((ContentEntityObject) anyObject(), anyString(), anyString());
        verify(contentPropertyManager, never()).setStringProperty((ContentEntityObject) anyObject(), anyString(), anyString());
    }
}
