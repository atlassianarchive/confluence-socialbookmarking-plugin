package com.atlassian.confluence.plugins.socialbookmarking;

import com.atlassian.confluence.content.render.xhtml.ConversionContext;
import com.atlassian.confluence.content.render.xhtml.XhtmlException;
import com.atlassian.confluence.content.render.xhtml.definition.PlainTextMacroBody;
import com.atlassian.confluence.pages.AbstractPage;
import com.atlassian.confluence.pages.Page;
import com.atlassian.confluence.xhtml.api.MacroDefinition;
import com.atlassian.confluence.xhtml.api.MacroDefinitionHandler;
import com.atlassian.confluence.xhtml.api.XhtmlContent;
import junit.framework.TestCase;
import org.mockito.Matchers;
import org.mockito.Mock;
import org.mockito.MockitoAnnotations;
import org.mockito.invocation.InvocationOnMock;
import org.mockito.stubbing.Answer;

import java.util.HashMap;
import java.util.Map;

import static org.mockito.Mockito.anyString;
import static org.mockito.Mockito.doAnswer;

public class BookmarkMacroParserTest extends TestCase
{
    @Mock
    private XhtmlContent xhtmlContent;

    private MacroDefinition macroDefinition;
    
    private DefaultBookmarkMacroParser bookmarkMacroParser;

    private AbstractPage abstractPage;

    protected void setUp() throws Exception
    {
        super.setUp();
        MockitoAnnotations.initMocks(this);

        macroDefinition = new MacroDefinition();
        abstractPage = new Page();

        bookmarkMacroParser = new DefaultBookmarkMacroParser(xhtmlContent);
    }

    @Override
    protected void tearDown() throws Exception
    {
        xhtmlContent = null;
        super.tearDown();
    }

    public void testRunMacroWithBookmarkMacroInPageContent() throws XhtmlException
    {
        final String macroBody = "body";
        final String url = "http://www.atlassian.com";
        
        macroDefinition.setName("bookmark");
        macroDefinition.setBody(new PlainTextMacroBody(macroBody));
        macroDefinition.setParameters(
                new HashMap<String, String>()
                {
                    {
                        put("url", url);
                    }
                }
        );

        doAnswer(
                new Answer()
                {
                    public Object answer(InvocationOnMock invocationOnMock) throws Throwable
                    {
                        MacroDefinitionHandler macroDefinitionHandler = (MacroDefinitionHandler) invocationOnMock.getArguments()[2];
                        macroDefinitionHandler.handle(macroDefinition);
                        return null;
                    }
                }
        ).when(xhtmlContent).handleMacroDefinitions(
                anyString(),
                Matchers.<ConversionContext>anyObject(),
                Matchers.<MacroDefinitionHandler>anyObject()
        );

        Map<String, String> macroParams = bookmarkMacroParser.getParameters(abstractPage);
        assertFalse(macroParams.isEmpty());

        assertEquals(url, macroParams.get("url"));
        assertEquals(macroBody, macroParams.get("description"));
    }
}
