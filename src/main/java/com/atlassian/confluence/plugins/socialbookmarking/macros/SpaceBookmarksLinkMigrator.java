package com.atlassian.confluence.plugins.socialbookmarking.macros;

import com.atlassian.confluence.content.render.xhtml.ConversionContext;
import com.atlassian.confluence.content.render.xhtml.definition.PlainTextMacroBody;
import com.atlassian.confluence.macro.xhtml.MacroMigration;
import com.atlassian.confluence.xhtml.api.MacroDefinition;
import com.atlassian.confluence.xhtml.api.XhtmlContent;
import org.apache.commons.lang.StringUtils;
import org.dom4j.Document;
import org.dom4j.io.DOMReader;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.w3c.tidy.Tidy;

import java.io.ByteArrayInputStream;
import java.io.ByteArrayOutputStream;
import java.io.PrintWriter;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;

public class SpaceBookmarksLinkMigrator implements MacroMigration
{
    private static final Logger LOG = LoggerFactory.getLogger(SpaceBookmarksLinkMigrator.class);

    private final XhtmlContent xhtmlContent;

    public SpaceBookmarksLinkMigrator(XhtmlContent xhtmlContent)
    {
        this.xhtmlContent = xhtmlContent;
    }

    public MacroDefinition migrate(MacroDefinition macroDefinition, ConversionContext conversionContext)
    {
        String bodyText = StringUtils.defaultString(macroDefinition.getBodyText());
        List<RuntimeException> conversionErrors = new ArrayList<RuntimeException>();

        try
        {
            final String xhtml = xhtmlContent.convertStorageToView(
                    xhtmlContent.convertWikiToStorage(bodyText, conversionContext, conversionErrors),
                    conversionContext
            );

            if (conversionErrors.isEmpty())
            {
                // Only change macro definition if there isn't any error in the conversion
                macroDefinition.setParameters(
                    new HashMap<String, String>(macroDefinition.getParameters())
                    {
                        {
                            String linkAlias = StringUtils.defaultString(getMacroBodyDocument(new StringBuilder("<div>").append(xhtml).append("</div>").toString()).getRootElement().getStringValue());
                            if (StringUtils.isNotBlank(linkAlias))
                                put("alias", linkAlias);
                        }
                    }
                );
                macroDefinition.setBody(new PlainTextMacroBody(""));
            }
            else
            {
                LOG.error(String.format("Error(s) encountered when converting wik mark below to storage:\n%s", bodyText));
                for (Exception conversionError : conversionErrors)
                    LOG.error("Conversion error", conversionError);
            }
        }
        catch (Exception e)
        {
            LOG.error(String.format("Unable to migrate wiki markup in bookmark macro:\n%s", bodyText), e);
        }

        return macroDefinition;
    }

    private Document getMacroBodyDocument(String macroBodyXhtmlCleaned)
    {
        ByteArrayOutputStream errorStream = new ByteArrayOutputStream();
        try
        {
            Tidy tidy = new Tidy();
            tidy.setQuiet(true);
            tidy.setErrout(new PrintWriter(errorStream));

            return new DOMReader().read(new Tidy().parseDOM(new ByteArrayInputStream(macroBodyXhtmlCleaned.getBytes()), null));
        }
        finally
        {
            LOG.error(String.format("There were some warnings parsing the specified HTML fragment\n%s", new String(errorStream.toByteArray())));
        }
    }
}
