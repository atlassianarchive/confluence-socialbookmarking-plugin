package com.atlassian.confluence.plugins.socialbookmarking;

import com.atlassian.confluence.core.ContentEntityObject;
import com.atlassian.confluence.core.ContentPropertyManager;
import com.atlassian.confluence.event.events.content.ContentEvent;
import com.atlassian.confluence.event.events.content.blogpost.BlogPostCreateEvent;
import com.atlassian.confluence.event.events.content.blogpost.BlogPostUpdateEvent;
import com.atlassian.confluence.event.events.content.page.PageCreateEvent;
import com.atlassian.confluence.event.events.content.page.PageUpdateEvent;
import com.atlassian.confluence.event.events.types.Updated;
import com.atlassian.confluence.plugins.socialbookmarking.macros.AbstractBookmarksMacro;
import com.atlassian.event.Event;
import com.atlassian.event.EventListener;

import java.util.Map;

public class BookmarkUpdateListener implements EventListener
{
    private static final Class[] HANDLED_EVENTS = new Class[]{
            PageCreateEvent.class,
            PageUpdateEvent.class,
            BlogPostCreateEvent.class,
            BlogPostUpdateEvent.class
    };

    private ContentPropertyManager contentPropertyManager;

    private BookmarkMacroParser bookmarkMacroParser;

    public Class[] getHandledEventClasses()
    {
        return HANDLED_EVENTS.clone();
    }

    public void handleEvent(Event anEvent)
    {
        ContentEvent contentEvent = (ContentEvent) anEvent;
        ContentEntityObject contentEntityObject = contentEvent.getContent();

        Map<String, String> bookmarkMacroParams = bookmarkMacroParser.getParameters(contentEntityObject);

        // Check the bookmark macro parser returned something, its
        // possible for people to create child pages of the bookmark
        // parent that don't have the bookmark macro on them.
        String url = bookmarkMacroParams.get(AbstractBookmarksMacro.URL_PARAM);
        if (url != null)
        {
            contentPropertyManager.setTextProperty(contentEntityObject, BookmarkUtils.URL_PROPERTY_KEY, url);
            contentPropertyManager.setStringProperty(contentEntityObject, BookmarkUtils.ISBOOKMARK_PROPERTY_KEY, Boolean.TRUE.toString());

            return;
        }

        // This page might have been a bookmark page that has been
        // moved, or the macro is now invalid so remove any bookmark
        // properties that are set
        if (contentEvent instanceof Updated)
        {
            contentPropertyManager.setTextProperty(contentEntityObject, BookmarkUtils.URL_PROPERTY_KEY, null);
            contentPropertyManager.setStringProperty(contentEntityObject, BookmarkUtils.ISBOOKMARK_PROPERTY_KEY, null);
        }
    }

    public void setBookmarkMacroParser(BookmarkMacroParser bookmarkMacroParser)
    {
        this.bookmarkMacroParser = bookmarkMacroParser;
    }

    public void setContentPropertyManager(ContentPropertyManager contentPropertyManager)
    {
        this.contentPropertyManager = contentPropertyManager;
    }
}

