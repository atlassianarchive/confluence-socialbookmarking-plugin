package com.atlassian.confluence.plugins.socialbookmarking;


import com.atlassian.confluence.content.render.xhtml.DefaultConversionContext;
import com.atlassian.confluence.content.render.xhtml.XhtmlException;
import com.atlassian.confluence.core.ContentEntityObject;
import com.atlassian.confluence.xhtml.api.MacroDefinition;
import com.atlassian.confluence.xhtml.api.MacroDefinitionHandler;
import com.atlassian.confluence.xhtml.api.XhtmlContent;
import org.apache.commons.lang.StringUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.util.HashMap;
import java.util.Map;

public class DefaultBookmarkMacroParser implements BookmarkMacroParser
{
    private static final Logger LOG = LoggerFactory.getLogger(DefaultBookmarkMacroParser.class);

    private XhtmlContent xhtmlContent;

    public DefaultBookmarkMacroParser(XhtmlContent xhtmlContent)
    {
        setXhtmlContent(xhtmlContent);
    }

    @SuppressWarnings("unused")
    public DefaultBookmarkMacroParser()
    {
        this(null);
    }

    public void setXhtmlContent(XhtmlContent xhtmlContent)
    {
        this.xhtmlContent = xhtmlContent;
    }

    public Map<String, String> getParameters(ContentEntityObject contentEntityObject)
    {
        BookmarkMacroHandler bookmarkMacroHandler = new BookmarkMacroHandler();
        try
        {
            xhtmlContent.handleMacroDefinitions(
                    contentEntityObject.getBodyAsString(),
                    new DefaultConversionContext(contentEntityObject.toPageContext()),
                    bookmarkMacroHandler
            );
        }
        catch (XhtmlException xhtmlError)
        {
            LOG.error(String.format("Unable to find bookmark macro parameters in content %s", contentEntityObject.toString()), xhtmlError);
        }

        return bookmarkMacroHandler.getParameters();
    }

    private class BookmarkMacroHandler implements MacroDefinitionHandler
    {
        private final Map<String, String> parameters;

        private BookmarkMacroHandler()
        {
            this.parameters = new HashMap<String, String>();
        }

        public Map<String, String> getParameters()
        {
            return parameters;
        }

        public void handle(MacroDefinition macroDefinition)
        {
            if (StringUtils.equals("bookmark", macroDefinition.getName()))
            {
                parameters.putAll(
                        macroDefinition.getParameters()
                );
                parameters.put("description", macroDefinition.getBodyText());
                // TODO: Decide if we really need descriptionExcerpt
//                parameters.put("descriptionExcerpt", macroDefinition.get);
            }
        }
    }
}
