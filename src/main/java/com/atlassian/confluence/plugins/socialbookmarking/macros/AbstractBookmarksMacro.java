package com.atlassian.confluence.plugins.socialbookmarking.macros;

import com.atlassian.confluence.content.render.xhtml.ConversionContext;
import com.atlassian.confluence.content.render.xhtml.DefaultConversionContext;
import com.atlassian.confluence.languages.LocaleManager;
import com.atlassian.confluence.macro.Macro;
import com.atlassian.confluence.macro.MacroExecutionException;
import com.atlassian.confluence.plugins.socialbookmarking.BookmarkUtils;
import com.atlassian.confluence.spaces.SpaceManager;
import com.atlassian.confluence.user.AuthenticatedUserThreadLocal;
import com.atlassian.confluence.util.i18n.I18NBean;
import com.atlassian.confluence.util.i18n.I18NBeanFactory;
import com.atlassian.renderer.RenderContext;
import com.atlassian.renderer.TokenType;
import com.atlassian.renderer.v2.RenderUtils;
import com.atlassian.renderer.v2.macro.BaseMacro;
import org.apache.commons.lang.BooleanUtils;
import org.apache.commons.lang.StringUtils;

import java.util.Map;

public abstract class AbstractBookmarksMacro extends BaseMacro implements Macro
{
    public final static String SPACES_PARAM = "spaces";
    public final static String SPACE_PARAM = "space";
    public final static String LABELS_PARAM = "labels";
    public final static String LABEL_PARAM = "label";
    public final static String CREATOR_PARAM = "creator";
    public final static String CREATORS_PARAM = "creators";
    public final static String MAX_PARAM = "max";
    public final static String SHOWLABELS_PARAM = "showLabels";
    public final static String SHOWDESCRIPTION_PARAM = "showDescription";
    public final static String SHOWAUTHOR_PARAM = "showAuthor";
    public final static String SHOWTITLE_PARAM = "showTitle";
    public final static String SHOWDATE_PARAM = "showDate";
    public final static String SORT_PARAM = "sort";
    public final static String SHOWRSS_PARAM = "showRss";
    public final static String SHOWADDBOOKMARK_PARAM = "showAddBookmark";
    public final static String SHOWLISTHEADER_PARAM = "showListHeader";
    public final static String SHOWSPACE_PARAM = "showSpace";
    public final static String SHOWCOMMENTS_PARAM = "showComments";
    public final static String SHOWEDIT_PARAM = "showEditLinks";
    public final static String REVERSE_PARAM = "reverseSort";
    public final static String SHOWVIEWLINK_PARAM = "showViewLink";
	public static final String URL_PARAM = "url";
    public static final String SPACE_KEYS_SEPARATOR_PARAM = "spaceKeysSeparator";
    
    protected BookmarkUtils bookmarkUtils;
    protected SpaceManager spaceManager;
    private LocaleManager localeManager;
    private I18NBeanFactory i18NBeanFactory;

    protected AbstractBookmarksMacro(BookmarkUtils bookmarkUtils, SpaceManager spaceManager, LocaleManager localeManager, I18NBeanFactory i18NBeanFactory)
    {
        setBookmarkUtils(bookmarkUtils);
        setSpaceManager(spaceManager);
        setLocaleManager(localeManager);
        setI18NBeanFactory(i18NBeanFactory);
    }

    public void setBookmarkUtils(BookmarkUtils bookmarkUtils)
    {
        this.bookmarkUtils = bookmarkUtils;
    }

    public void setSpaceManager(SpaceManager spaceManager)
    {
        this.spaceManager = spaceManager;
    }

    public void setLocaleManager(LocaleManager localeManager)
    {
        this.localeManager = localeManager;
    }

    public void setI18NBeanFactory(I18NBeanFactory i18NBeanFactory)
    {
        this.i18NBeanFactory = i18NBeanFactory;
    }

    private I18NBean getI18nBean()
    {
        return i18NBeanFactory.getI18NBean(localeManager.getLocale(AuthenticatedUserThreadLocal.getUser()));
    }

    String getText(String i18nKey)
    {
        return getI18nBean().getText(i18nKey);
    }



    /**
	 * Populates the second map with values of SHOWXXX flags parsed from the first.
	 * 
	 * @param params
     * The {@link Map} that <i>may</i> contain SHOWXXX flags. If a flag does not exist here, it's value will default to <tt>false</tt>.
	 * @param velocityContext
     * The {@link Map} which the parsed values are populated to.
	 * @return
     * Reference to the second {@link Map}.
	 */
    public static Map<String, Object> getParams(Map<String, ?> params, Map<String, Object> velocityContext)
    {
        // Display options - default to true if parameter not defined
        boolean showTitle = BooleanUtils.toBoolean(StringUtils.defaultIfEmpty((String) params.get(SHOWTITLE_PARAM), Boolean.TRUE.toString()));
        boolean showAuthor = BooleanUtils.toBoolean(StringUtils.defaultIfEmpty((String) params.get(SHOWAUTHOR_PARAM), Boolean.TRUE.toString()));
        boolean showLabels = BooleanUtils.toBoolean(StringUtils.defaultIfEmpty((String) params.get(SHOWLABELS_PARAM), Boolean.TRUE.toString()));
        boolean showDate = BooleanUtils.toBoolean(StringUtils.defaultIfEmpty((String) params.get(SHOWDATE_PARAM), Boolean.TRUE.toString()));
        boolean showDescription = BooleanUtils.toBoolean(StringUtils.defaultIfEmpty((String) params.get(SHOWDESCRIPTION_PARAM), Boolean.TRUE.toString()));
        boolean showSpace = BooleanUtils.toBoolean(StringUtils.defaultIfEmpty((String) params.get(SHOWSPACE_PARAM), Boolean.TRUE.toString()));
        boolean showComments = BooleanUtils.toBoolean(StringUtils.defaultIfEmpty((String) params.get(SHOWCOMMENTS_PARAM), Boolean.TRUE.toString()));
        boolean showEditLinks = BooleanUtils.toBoolean(StringUtils.defaultIfEmpty((String) params.get(SHOWEDIT_PARAM), Boolean.TRUE.toString()));
        boolean showListHeader = BooleanUtils.toBoolean(StringUtils.defaultIfEmpty((String) params.get(SHOWLISTHEADER_PARAM), Boolean.TRUE.toString()));
        boolean showRss = BooleanUtils.toBoolean(StringUtils.defaultIfEmpty((String) params.get(SHOWRSS_PARAM), Boolean.TRUE.toString()));
        boolean showViewLink = BooleanUtils.toBoolean(StringUtils.defaultIfEmpty((String) params.get(SHOWVIEWLINK_PARAM), Boolean.TRUE.toString()));
        boolean showAddBookmark = BooleanUtils.toBoolean(StringUtils.defaultIfEmpty((String) params.get(SHOWADDBOOKMARK_PARAM), Boolean.TRUE.toString()));

        velocityContext.put(SHOWTITLE_PARAM, showTitle);
        velocityContext.put(SHOWAUTHOR_PARAM, showAuthor);
        velocityContext.put(SHOWLABELS_PARAM, showLabels);
        velocityContext.put(SHOWDATE_PARAM, showDate);
        velocityContext.put(SHOWDESCRIPTION_PARAM, showDescription);
        velocityContext.put(SHOWSPACE_PARAM, showSpace);
        velocityContext.put(SHOWCOMMENTS_PARAM, showComments);
        velocityContext.put(SHOWEDIT_PARAM, showEditLinks);
        velocityContext.put(SHOWLISTHEADER_PARAM, showListHeader);
        velocityContext.put(SHOWRSS_PARAM, showRss);
        velocityContext.put(SHOWVIEWLINK_PARAM, showViewLink);
        velocityContext.put(SHOWADDBOOKMARK_PARAM, showAddBookmark);

        /* Copy any other key-value pair into the Velocity context map */
        for (Map.Entry<String, ?> entry : params.entrySet())
            if (!velocityContext.containsKey(entry.getKey()))
                velocityContext.put(entry.getKey(), entry.getValue());

        return velocityContext;
    }

    @Override
    public TokenType getTokenType(Map parameters, String body, RenderContext context)
    {
        return TokenType.BLOCK;
    }

    public OutputType getOutputType()
    {
        return OutputType.BLOCK;
    }

    public String execute(Map parameters, String body, RenderContext renderContext)
    {
        return execute((Map<String, String>) parameters, body, new DefaultConversionContext(renderContext));
    }

    public abstract String execute(Map<String, String> parameters, String body, ConversionContext conversionContext);
}
