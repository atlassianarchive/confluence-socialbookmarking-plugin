package com.atlassian.confluence.plugins.socialbookmarking;

public class BookmarkDateComparator extends AbstractBookmarkComparator
{
    @Override
    protected int compareInternal(Bookmark leftBookmark, Bookmark rightBookmark)
    {
        /* This comparator sorts in descending order. See https://studio.plugins.atlassian.com/browse/MARK-130 */
        return -leftBookmark.getAddedDate().compareTo(rightBookmark.getAddedDate());
    }
}
