package com.atlassian.confluence.plugins.socialbookmarking;

import java.util.List;

public class Bookmarks
{
    private boolean hasNextPage;

    private List<Bookmark> bookmarkList;

    public List<Bookmark> getBookmarkList()
    {
        return bookmarkList;
    }

    public void setBookmarkList(List<Bookmark> bookmarkList)
    {
        this.bookmarkList = bookmarkList;
    }

    public boolean isHasNextPage()
    {
        return hasNextPage;
    }

    public void setHasNextPage(boolean hasNextPage)
    {
        this.hasNextPage = hasNextPage;
    }


}
