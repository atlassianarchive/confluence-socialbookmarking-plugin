package com.atlassian.confluence.plugins.socialbookmarking.macros;

import com.atlassian.confluence.content.render.xhtml.ConversionContext;
import com.atlassian.confluence.core.ContentEntityObject;
import com.atlassian.confluence.languages.LocaleManager;
import com.atlassian.confluence.links.linktypes.PageLink;
import com.atlassian.confluence.macro.Macro;
import com.atlassian.confluence.pages.PageManager;
import com.atlassian.confluence.plugin.services.VelocityHelperService;
import com.atlassian.confluence.plugins.socialbookmarking.Bookmark;
import com.atlassian.confluence.plugins.socialbookmarking.BookmarkUtils;
import com.atlassian.confluence.security.Permission;
import com.atlassian.confluence.security.PermissionManager;
import com.atlassian.confluence.setup.BootstrapManager;
import com.atlassian.confluence.spaces.SpaceManager;
import com.atlassian.confluence.user.AuthenticatedUserThreadLocal;
import com.atlassian.confluence.util.i18n.I18NBeanFactory;
import com.atlassian.plugin.webresource.UrlMode;
import com.atlassian.plugin.webresource.WebResourceUrlProvider;
import com.atlassian.renderer.RenderContextOutputType;
import com.atlassian.renderer.links.Link;
import com.atlassian.renderer.links.LinkResolver;
import com.atlassian.renderer.v2.RenderMode;
import com.atlassian.renderer.v2.RenderUtils;
import com.atlassian.user.User;
import org.apache.commons.lang.StringUtils;

import java.util.Map;

public class BookmarkMacro extends AbstractBookmarksMacro implements Macro
{
    private static final String PAGE_PARAM = "page";

    private PermissionManager permissionManager;

    private BootstrapManager bootstrapManager;

    private PageManager pageManager;

    private LinkResolver linkResolver;

    private WebResourceUrlProvider webResourceUrlProvider;

    private VelocityHelperService velocityHelperService;

    public BookmarkMacro(BookmarkUtils bookmarkUtils, SpaceManager spaceManager, LocaleManager localeManager, I18NBeanFactory i18NBeanFactory, PermissionManager permissionManager, BootstrapManager bootstrapManager, PageManager pageManager, LinkResolver linkResolver, WebResourceUrlProvider webResourceUrlProvider, VelocityHelperService velocityHelperService)
    {
        super(bookmarkUtils, spaceManager, localeManager, i18NBeanFactory);
        setPermissionManager(permissionManager);
        setBootstrapManager(bootstrapManager);
        setPageManager(pageManager);
        setLinkResolver(linkResolver);
        setWebResourceUrlProvider(webResourceUrlProvider);
        setVelocityHelperService(velocityHelperService);
    }

    @SuppressWarnings("unused")
    public BookmarkMacro()
    {
        this(null, null, null, null, null, null, null, null, null, null);
    }

    public void setPermissionManager(PermissionManager permissionManager)
    {
        this.permissionManager = permissionManager;
    }

    public void setBootstrapManager(BootstrapManager bootstrapManager)
    {
        this.bootstrapManager = bootstrapManager;
    }

    public void setPageManager(PageManager pageManager)
    {
        this.pageManager = pageManager;
    }

    public void setLinkResolver(LinkResolver linkResolver)
    {
        this.linkResolver = linkResolver;
    }

    public void setWebResourceUrlProvider(WebResourceUrlProvider webResourceUrlProvider)
    {
        this.webResourceUrlProvider = webResourceUrlProvider;
    }

    public void setVelocityHelperService(VelocityHelperService velocityHelperService)
    {
        this.velocityHelperService = velocityHelperService;
    }

    public String execute(Map<String, String> params, String body, ConversionContext conversionContext)
    {
        ContentEntityObject contentEntityObject = conversionContext.getEntity();

        if (bookmarkUtils.isSupportedContentType(contentEntityObject))
        {
            User authenticatedUser = AuthenticatedUserThreadLocal.getUser();
            String url = params.get(URL_PARAM);
            Bookmark bookmark;
            String spaceKey;

            if (pageManager.getPage(bookmarkUtils.getSpaceFromContentEntity(contentEntityObject).getKey(), url) == null &&
                    !bookmarkUtils.isValidProtocol(url))
            {
                return RenderUtils.blockError(getText("bookmark.error.invalidpagelink"), "");
            }
            else if (StringUtils.isNotBlank(params.get(PAGE_PARAM)))
            {
                Link resolvedLink = linkResolver.createLink(contentEntityObject.toPageContext(), params.get(PAGE_PARAM));
                if (resolvedLink instanceof PageLink)
                {
                    PageLink pageLink = (PageLink) resolvedLink;
                    contentEntityObject = pageManager.getPage(pageLink.getSpaceKey(), pageLink.getTitle());

                    if (permissionManager.hasPermission(authenticatedUser, Permission.VIEW, contentEntityObject))
                    {
                        bookmark = bookmarkUtils.getBookmark(contentEntityObject.getId());
                        spaceKey = pageLink.getSpaceKey();
                    }
                    else
                    {
                        return RenderUtils.blockError(getText("bookmark.error.invalidpagelink"), "");
                    }
                }
                else
                {
                    return RenderUtils.blockError(getText("bookmark.error.invalidpagelink"), "");
                }
            }
            else
            {
                /*
                 * MARK-42 - Depending on BookmarkUpdateListener to update the properties of the page to indicate
                 * that it is a bookmark page can lead to NPEs. That happens if mail listeners are executed before
                 * BookmarkUpdateListener.
                 *
                 * If this macro gets executed in the current page and control goes here, just convert the
                 * page to a bookmark.
                 */

                bookmark = bookmarkUtils.convertPageToBookmark(contentEntityObject, authenticatedUser);
                bookmark.setDescription(body);

                spaceKey = bookmarkUtils.getSpaceFromContentEntity(contentEntityObject).getKey();
            }

            // Disable editing and removal if rendered in preview mode
            if (RenderContextOutputType.PREVIEW.equals(conversionContext.getOutputType()))
            {
                bookmark.setEditPermission(false);
                bookmark.setRemovePermission(false);
            }

            @SuppressWarnings("unchecked")
            Map<String, Object> velocityContext = getParams(params, velocityHelperService.createDefaultVelocityContext());

            velocityContext.put("url", url);
            velocityContext.put("body", body);
            velocityContext.put("bookmark", bookmark);
            velocityContext.put("id", bookmark.getId());
            velocityContext.put("resourcePath", bootstrapManager.getWebAppContextPath() + "/download/resources/com.atlassian.confluence.plugins.socialbookmarking/resources");
            velocityContext.put("spaceName", bookmark.getSpaceName());
            velocityContext.put("spaceKey", spaceKey);
            velocityContext.put("showViewLink", Boolean.FALSE);
            velocityContext.put("showComments", Boolean.FALSE);
            velocityContext.put("returnUserToBookmarkPage", Boolean.TRUE);
            velocityContext.put("editPermission", permissionManager.hasPermission(AuthenticatedUserThreadLocal.getUser(), Permission.EDIT, contentEntityObject));
            velocityContext.put("macroBodyHtml", velocityHelperService.getRenderedTemplate("templates/plugins/socialbookmarking/viewbookmark.vm", velocityContext));
            velocityContext.put("bookmarkIconUrl", webResourceUrlProvider.getStaticPluginResourceUrl("com.atlassian.confluence.plugins.socialbookmarking:bookmarks", "images/BookmarkIcon.gif", UrlMode.ABSOLUTE));

            return velocityHelperService.getRenderedTemplate("templates/plugins/socialbookmarking/bookmark.vm", velocityContext);
        }
        else
        {
            return RenderUtils.blockError(
                    getText("bookmark.error.unsupportedcontenttype"), ""
            );
        }
    }
    
    public RenderMode getBodyRenderMode()
    {
        return RenderMode.NO_RENDER;
    }

    public boolean hasBody()
    {
        return true;
    }

    public BodyType getBodyType()
    {
        return BodyType.RICH_TEXT;
    }
}
