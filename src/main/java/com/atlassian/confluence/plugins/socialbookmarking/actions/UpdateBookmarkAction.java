package com.atlassian.confluence.plugins.socialbookmarking.actions;

import com.atlassian.confluence.content.render.xhtml.links.WebLink;
import com.atlassian.confluence.core.ConfluenceActionSupport;
import com.atlassian.confluence.labels.Label;
import com.atlassian.confluence.labels.LabelParser;
import com.atlassian.confluence.pages.AbstractPage;
import com.atlassian.confluence.pages.Page;
import com.atlassian.confluence.pages.PageManager;
import com.atlassian.confluence.plugins.socialbookmarking.Bookmark;
import com.atlassian.confluence.plugins.socialbookmarking.BookmarkUtils;
import com.atlassian.confluence.security.CaptchaAware;
import com.atlassian.confluence.security.CaptchaManager;
import com.atlassian.confluence.security.Permission;
import com.atlassian.confluence.security.SpacePermission;
import com.atlassian.confluence.setup.BootstrapManager;
import com.atlassian.confluence.spaces.Space;
import com.atlassian.confluence.spaces.SpaceManager;
import com.atlassian.confluence.spaces.SpaceType;
import com.atlassian.confluence.spaces.SpacesQuery;
import com.atlassian.confluence.user.AuthenticatedUserThreadLocal;
import com.atlassian.confluence.user.ConfluenceUser;
import com.atlassian.confluence.util.LabelUtil;
import com.atlassian.plugin.webresource.UrlMode;
import com.atlassian.plugin.webresource.WebResourceUrlProvider;
import com.atlassian.user.User;
import com.google.common.annotations.VisibleForTesting;
import org.apache.commons.lang.StringUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.util.*;

public class UpdateBookmarkAction extends ConfluenceActionSupport implements CaptchaAware
{
    private static final Logger LOG = LoggerFactory.getLogger(UpdateBookmarkAction.class);
    
    private static final String REDIRECT_VIEW = "view";
    private static final String REDIRECT_SPACE = "spacebookmarks";
    private static final String REDIRECT_BOOKMARKURL = "bookmarkurl";

    @VisibleForTesting
    static final String URL_REQUIRED = "bookmark.url.required";

    @VisibleForTesting
    static final String URL_INVALID = "bookmark.url.invalid";

    private String title;
    private String description;
    private String url;
    private String spaceKey;
    private String labelsString;
    private String bookmarkPageId;
    private String fromPageId;
    private boolean returnUserToBookmarkPage;

    /**
     * page to be passed to velocity template for getting suggested labels
     */
    private Page page;

    /**
     * Dodgy string to hold the bookmark url when we need to redirect back to
     * the bookmarked page after the user has saved the page. Yes we should just
     * be able to use the url parameter in this class but the ${url} variable in
     * atlassian-plugin.xml gets filtered by maven so we need a different name.
     */
    private String bookmarkRedirectUrl;

    private String redirect;
    
    private String titleText;

    private SpaceManager spaceManager;
    private BookmarkUtils bookmarkUtils;
    private PageManager pageManager;
    private BootstrapManager bootstrapManager;
    private CaptchaManager captchaManager;
    private WebResourceUrlProvider webResourceUrlProvider;

    private String resourcePath;
    private static final List<String> SCHEMA_BLACKLIST = Collections.unmodifiableList(Arrays.asList("javascript", "vbscript"));



    public String execute() throws Exception
    {
        resourcePath = bootstrapManager.getWebAppContextPath() + "/download/resources/com.atlassian.confluence.plugins.socialbookmarking/resources";
        
        Space bookmarkSpace = StringUtils.isBlank(getSpaceKey())
                ? spaceManager.getPersonalSpace(AuthenticatedUserThreadLocal.get())
                : spaceManager.getSpace(getSpaceKey());
        Page bookmarkParent = bookmarkUtils.getBookmarkParent(bookmarkSpace, true);
        Page bookmarkPage = StringUtils.isBlank(getBookmarkPageId()) ? new Page() : pageManager.getPage(Long.parseLong(getBookmarkPageId()));
        String bookmarkTitle = validateTitle(StringUtils.defaultString(StringUtils.trim(getTitle())));
        String bookmarkWikiContent = getBookmarkWikiContent();

        // Do not modify bookmarkPage anywhere before the if-else block below because it may need to be cloned later as the "original" page.
        if (!bookmarkPage.isPersistent())
        {
            bookmarkPage.setSpace(bookmarkSpace);
            bookmarkPage.setTitle(bookmarkTitle);
            setBookmarkPageContent(bookmarkPage, bookmarkWikiContent);
            bookmarkParent.addChild(bookmarkPage);
            
            pageManager.saveContentEntity(bookmarkPage, null);

            if (LOG.isDebugEnabled())
                LOG.debug(String.format("Create new bookmark page with ID %s", bookmarkPage.getIdAsString()));
        }
        else
        {
            Page bookmarkPageOriginal = (Page) bookmarkPage.clone();
            
            labelManager.removeAllLabels(bookmarkPage); // Clear all labels first

            // Only save if either the title or content has changed

            if (!(StringUtils.equals(bookmarkTitle, bookmarkPageOriginal.getTitle()) && (StringUtils.equals(bookmarkWikiContent, bookmarkPageOriginal.getBodyAsString()))))
            {
                bookmarkPage.setTitle(bookmarkTitle);
                setBookmarkPageContent(bookmarkPage, bookmarkWikiContent);
                pageManager.saveContentEntity(bookmarkPage, bookmarkPageOriginal, null);

                if (LOG.isDebugEnabled())
                    LOG.debug(String.format("Updated bookmark page with ID %s", bookmarkPage.getIdAsString()));
            }

            Space oldBookmarkSpace = bookmarkPage.getSpace();
            if (!StringUtils.equals(oldBookmarkSpace.getKey(), bookmarkSpace.getKey()))
                pageManager.movePageAsChild(bookmarkPage, bookmarkParent);
        }

        for (String label : StringUtils.defaultString(getLabelsString()).split("[, ]"))
            if (StringUtils.isNotBlank(label))
                labelManager.addLabel(bookmarkPage, LabelParser.parse(label).toLabel());

        return getRedirectResult();
    }

    /* For testing, so it could be overridden */
    void setBookmarkPageContent(Page bookmarkPage, String bookmarkPageContent)
    {
        // Since there is no way to build macro XHTML tags, we rely on this so the migration would be performed
        bookmarkPage.setContent(bookmarkPageContent);
    }

    private String getBookmarkWikiContent()
    {
        return new StringBuilder("{bookmark:url=").append(getUrl()).append("|htmlbody=true}").append(StringUtils.defaultString(getDescription())).append("{bookmark}").toString();
    }

    public String getCancelResult()
    {
        return getRedirectResult(); /* Fix for MARK-84 */
    }

    protected String getRedirectResult()
    {
        String redirect = getRedirect();

        if (StringUtils.equals(redirect, REDIRECT_VIEW))
        {
            if (isReturnUserToBookmarkPage() && StringUtils.isBlank(getFromPageId()))
                setFromPageId(getBookmarkPageId());

            if (!isReturnUserToBookmarkPage() && StringUtils.isBlank(getFromPageId()))
                setRedirect(REDIRECT_SPACE);
        }

        if (StringUtils.equals(redirect, REDIRECT_BOOKMARKURL))
            setBookmarkRedirectUrl(getUrl());

        return getRedirect();
    }

    @Override
    public String doDefault() throws Exception
    {
        if (StringUtils.isBlank(getBookmarkPageId()))
        {
            setTitleText(getText("bookmark.addbookmark.title"));

            if (StringUtils.isBlank(getSpaceKey()))
            {
                Space personalSpace = spaceManager.getPersonalSpace(getRemoteUser());
                if (null != personalSpace)
                    setSpaceKey(personalSpace.getKey());
            }

            setPage(new Page());
        }
        else
        {
            setTitleText(getText("bookmark.updatebookmark.title"));

            Page bookmarkPage = pageManager.getPage(Long.parseLong(getBookmarkPageId()));
            setPage(bookmarkPage);

            Bookmark bookmark = bookmarkUtils.convertPageToBookmark(bookmarkPage, AuthenticatedUserThreadLocal.getUser());

            setTitle(bookmark.getTitle());
            setUrl(bookmark.getUrl());
            setSpaceKey(bookmarkPage.getSpaceKey());
            setDescription(bookmark.getDescription());

            Collection<Label> bookmarkLabels = bookmark.getLabels();
            if (null != bookmarkLabels)
            {
                StringBuilder labelssStringBuilder = new StringBuilder();
                for (Label label : bookmarkLabels)
                {
                    if (labelssStringBuilder.length() > 0)
                        labelssStringBuilder.append(' ');

                    labelssStringBuilder.append(label);
                }
                setLabelsString(labelssStringBuilder.toString());
            }
        }
        
        return INPUT;
    }

    @Override
    public void validate()
    {
        super.validate();
        
        Space bookmarkSpace = spaceManager.getSpace(StringUtils.defaultString(getSpaceKey()));
        User currentUser = AuthenticatedUserThreadLocal.getUser();

        if (null == bookmarkSpace)
            bookmarkSpace = spaceManager.getPersonalSpace(currentUser);
        if (null == bookmarkSpace)
            addActionError(getText("bookmark.select.space"));
        else
        {
            String bookmarkTitle = validateTitle(StringUtils.trim(getTitle()));
            if (StringUtils.isBlank(bookmarkTitle))
            {
                addActionError(getText("bookmark.title.required"));
            }
            else
            {
                if (!AbstractPage.isValidPageTitle(bookmarkTitle))
                {
                    addActionError(getText("bookmark.title.invalid"));
                }
                else
                {
                    if (StringUtils.isBlank(getBookmarkPageId()))
                    {
                        if (!permissionManager.hasCreatePermission(currentUser, bookmarkSpace, Page.class))
                            addActionError(new StringBuilder(getText("bookmark.no.permission.to.create")).append(' ').append(bookmarkSpace.getName()).append(' ').append(getText("bookmark.space")).toString());
                        else if (null != pageManager.getPage(bookmarkSpace.getKey(), bookmarkTitle))
                            addActionError(getText("bookmark.page.already.exists"));
                    }
                    else
                    {
                        Page bookmarkPage = pageManager.getPage(Long.parseLong(getBookmarkPageId()));
                        if (!permissionManager.hasPermission(currentUser, Permission.EDIT, bookmarkPage))
                            addActionError(getText("bookmark.no.permission.to.edit"));
                        else
                        {
                            Page existingPageWithSameTitle = pageManager.getPage(bookmarkSpace.getKey(), bookmarkTitle);
                            if (null != existingPageWithSameTitle && bookmarkPage.getId() != existingPageWithSameTitle.getId())
                                addActionError(getText("bookmark.page.already.exists"));
                        }
                    }

                    validateUrl(getUrl());

                    /* Labels validation */
                    if (!LabelUtil.isValidLabelNames(getLabelsString()))
                        addFieldError("labelsString", getText(getText("bookmark.labels.invalid", new String[]{LabelParser.getInvalidCharactersAsString()})));

                    if (!LabelUtil.isValidLabelLengths(getLabelsString()))
                        addFieldError("labelsString", getText("page.label.too.long", new Object[]{new Integer(LabelParser.MAX_LABEL_NAME_LENGTH)}));
                }
            }
        }
    }

    /**
     * Helper method to return a title with only valid characters.
     */
    String validateTitle(String title)
    {

        StringBuffer ret = new StringBuffer();
        if (title != null)
        {
            for (int i = 0; i < title.length(); i++)
            {
                if (AbstractPage.isValidPageTitleCharacter(title.charAt(i)))
                {
                    ret.append(title.charAt(i));
                }
            }

            if (LOG.isDebugEnabled() && !StringUtils.equals(ret.toString(), title))
                LOG.debug(String.format("Original title: %s, valid title: %s", title, ret.toString()));
        }

        return ret.toString();
    }

    @VisibleForTesting
    void validateUrl(String url)
    {
        if (StringUtils.isBlank(url))
        {
            addActionError(getText(URL_REQUIRED));
        }
        else if (!WebLink.isValidURL(url))
        {
            addActionError(getText(URL_INVALID));
        }
    }

    @SuppressWarnings("unused")
    public void setBookmarkUtils(BookmarkUtils bookmarkUtils)
    {
        this.bookmarkUtils = bookmarkUtils;
    }

    public String getBookmarkPageId()
    {
        return bookmarkPageId;
    }

    @SuppressWarnings("unused")
    public void setBookmarkPageId(String bookmarkPageId)
    {
        this.bookmarkPageId = bookmarkPageId;
    }

    public String getDescription()
    {
        return description;
    }

    @SuppressWarnings("unused")
    public void setDescription(String description)
    {
        this.description = description;
    }

    @SuppressWarnings("unused")
    public void setPageManager(PageManager pageManager)
    {
        this.pageManager = pageManager;
    }

    public String getSpaceKey()
    {
        return spaceKey;
    }

    public void setSpaceKey(String spaceKey)
    {
        this.spaceKey = spaceKey;
    }

    public void setSpaceManager(SpaceManager spaceManager)
    {
        this.spaceManager = spaceManager;
    }

    public String getLabelsString()
    {
        return labelsString;
    }

    @SuppressWarnings("unused")
    public void setLabelsString(String labels)
    {
        this.labelsString = labels;
    }

    public String getTitle()
    {
        return title;
    }

    public void setTitle(String title)
    {
        this.title = title;
    }

    public String getUrl()
    {
        return url;
    }

    public void setUrl(String url)
    {
        this.url = url;
    }

    @SuppressWarnings("unused")
    public String getTitleText()
    {
        return titleText;
    }

    public void setTitleText(String titleText)
    {
        this.titleText = titleText;
    }

    public String getFromPageId() 
    {
    	return fromPageId;
    }
    
    public void setFromPageId(String fromPageId)
    {
    	this.fromPageId = fromPageId;
    }

    @SuppressWarnings("unused")
    public void setReturnUserToBookmarkPage(boolean returnUserToBookmarkPage)
    {
        this.returnUserToBookmarkPage = returnUserToBookmarkPage;
    }

    public boolean isReturnUserToBookmarkPage()
    {
        return returnUserToBookmarkPage;
    }

    @SuppressWarnings({"unchecked", "unused"})
	public Collection<Space> getValidSpaces()
    {
        ConfluenceUser currentUser = AuthenticatedUserThreadLocal .get();
        
        Collection<Space> editableSpaces = new ArrayList<Space>();
        editableSpaces.addAll(spaceManager.getAllSpaces(SpacesQuery.newQuery()
                                                                   .forUser(currentUser)
                                                                   .withPermission(SpacePermission.CREATEEDIT_PAGE_PERMISSION)
                                                                   .withSpaceType(SpaceType.PERSONAL)
                                                                   .build()));
        editableSpaces.addAll(spaceManager.getAllSpaces(SpacesQuery.newQuery()
                .forUser(currentUser)
                .withPermission(SpacePermission.CREATEEDIT_PAGE_PERMISSION)
                .withSpaceType(SpaceType.GLOBAL)
                .build()));

        return editableSpaces;
    }

    public String getRedirect()
    {
        return StringUtils.defaultString(redirect, REDIRECT_BOOKMARKURL);
    }

    @SuppressWarnings("unused")
    public void setRedirect(String redirect)
    {
        this.redirect = redirect;
    }

    public String getResourcePath()
    {
        return resourcePath;
    }


    public void setBootstrapManager(BootstrapManager bootstrapManager)
    {
        this.bootstrapManager = bootstrapManager;
        super.setBootstrapManager(bootstrapManager);
    }

    @SuppressWarnings("unused")
    public Page getPage()
    {
        return page;
    }
    
    public void setPage(Page page)
    {
        this.page = page;
    }

    public String getBookmarkRedirectUrl()
    {
        return bookmarkRedirectUrl;
    }

    public void setBookmarkRedirectUrl(String bookmarkRedirectUrl)
    {
        this.bookmarkRedirectUrl = bookmarkRedirectUrl;
    }

    /**
     * For <a href="http://developer.atlassian.com/jira/browse/MARK-42">MARK-42</a>
     *
     * @return This method returns <code>true</code> always. Otherwise, labels will break in Confluence 2.8
     */
    public boolean isLabelsShowing()
    {
        return true;
    }

    /**
     * For <a href="http://developer.atlassian.com/jira/browse/MARK-42">MARK-42</a>
     *
     * @return Returns a page ID so that suggested labels can be shown.
     */
    @SuppressWarnings("unused")
    public long getPageId()
    {
        return null == page ? 0 : page.getId();
    }

    public CaptchaManager getCaptchaManager()
    {
        return captchaManager;
    }

    @SuppressWarnings("unused")
    public void setCaptchaManager(CaptchaManager captchaManager)
    {
        this.captchaManager = captchaManager;
    }

    @SuppressWarnings("unused")
    public void setWebResourceUrlProvider(WebResourceUrlProvider webResourceUrlProvider)
    {
        this.webResourceUrlProvider = webResourceUrlProvider;
    }

    @SuppressWarnings("unused")
    public String getStaticResourcePrefix()
    {
        return webResourceUrlProvider.getStaticResourcePrefix(UrlMode.ABSOLUTE);
    }

    public String getStaticPluginResourceUrl()
    {
        return webResourceUrlProvider.getStaticPluginResourceUrl("com.atlassian.confluence.plugins.socialbookmarking:bookmarks", "images/BookmarkIcon.gif", UrlMode.ABSOLUTE);
    }
}
