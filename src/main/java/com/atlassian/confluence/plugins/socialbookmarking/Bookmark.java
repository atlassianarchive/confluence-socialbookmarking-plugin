package com.atlassian.confluence.plugins.socialbookmarking;

import com.atlassian.confluence.labels.Label;
import com.atlassian.confluence.velocity.htmlsafe.HtmlSafe;
import com.atlassian.user.User;

import java.util.Date;
import java.util.List;

public class Bookmark
{
    private String title;
    private String description;
    private String url;
    private List<Label> labels;
    private Date addedDate;
    private String id;
    private User creator;
    private String spaceName;
    private String spaceUrlPath;
    private int commentCount;

    private boolean editPermission = false;
    private boolean removePermission = false;

    @HtmlSafe
    public String getDescription()
    {
        return description;
    }

    public void setDescription(String description)
    {
        this.description = description;
    }

    public List<Label> getLabels()
    {
        return labels;
    }

    public void setLabels(List<Label> labels)
    {
        this.labels = labels;
    }

    public String getTitle()
    {
        return title;
    }

    public void setTitle(String title)
    {
        this.title = title;
    }

    public String getUrl()
    {
        return url;
    }

    public void setUrl(String url)
    {
        this.url = url;
    }

    public Date getAddedDate()
    {
        return new Date(addedDate.getTime());
    }

    public void setAddedDate(Date addedDate)
    {
        this.addedDate = new Date(addedDate.getTime());
    }

    public String getId()
    {
        return id;
    }

    public void setId(String id)
    {
        this.id = id;
    }

    public boolean isEditPermission()
    {
        return editPermission;
    }

    public void setEditPermission(boolean editPermission)
    {
        this.editPermission = editPermission;
    }

    public boolean isRemovePermission()
    {
        return removePermission;
    }

    public void setRemovePermission(boolean removePermission)
    {
        this.removePermission = removePermission;
    }

    public User getCreator()
    {
        return creator;
    }

    public void setCreator(User creator)
    {
        this.creator = creator;
    }

    public String getSpaceName()
    {
        return spaceName;
    }

    public void setSpaceName(String spaceName)
    {
        this.spaceName = spaceName;
    }

    public int getCommentCount()
    {
        return commentCount;
    }

    public void setCommentCount(int commentCount)
    {
        this.commentCount = commentCount;
    }

    public String getSpaceUrlPath()
    {
        return spaceUrlPath;
    }

    public void setSpaceUrlPath(String spaceUrlPath)
    {
        this.spaceUrlPath = spaceUrlPath;
    }
}
