package com.atlassian.confluence.plugins.socialbookmarking;

import com.atlassian.confluence.content.render.xhtml.DefaultConversionContext;
import com.atlassian.confluence.core.ContentEntityObject;
import com.atlassian.confluence.core.ContentPropertyManager;
import com.atlassian.confluence.labels.Label;
import com.atlassian.confluence.labels.LabelManager;
import com.atlassian.confluence.labels.Labelable;
import com.atlassian.confluence.languages.LocaleManager;
import com.atlassian.confluence.pages.AbstractPage;
import com.atlassian.confluence.pages.BlogPost;
import com.atlassian.confluence.pages.Comment;
import com.atlassian.confluence.pages.Draft;
import com.atlassian.confluence.pages.Page;
import com.atlassian.confluence.pages.PageManager;
import com.atlassian.confluence.plugin.services.VelocityHelperService;
import com.atlassian.confluence.plugins.socialbookmarking.macros.AbstractBookmarksMacro;
import com.atlassian.confluence.security.Permission;
import com.atlassian.confluence.security.PermissionManager;
import com.atlassian.confluence.setup.settings.SettingsManager;
import com.atlassian.confluence.spaces.Space;
import com.atlassian.confluence.spaces.SpaceManager;
import com.atlassian.confluence.spaces.SpaceType;
import com.atlassian.confluence.spaces.SpacesQuery;
import com.atlassian.confluence.themes.PageHelper;
import com.atlassian.confluence.themes.ThemeHelper;
import com.atlassian.confluence.user.AuthenticatedUserThreadLocal;
import com.atlassian.confluence.user.UserAccessor;
import com.atlassian.confluence.util.GeneralUtil;
import com.atlassian.confluence.util.i18n.I18NBean;
import com.atlassian.confluence.util.i18n.I18NBeanFactory;
import com.atlassian.confluence.xhtml.api.XhtmlContent;
import com.atlassian.user.User;
import com.google.common.base.Function;
import com.google.common.base.Predicates;
import com.google.common.collect.Collections2;
import com.sun.syndication.feed.synd.SyndCategory;
import com.sun.syndication.feed.synd.SyndCategoryImpl;
import com.sun.syndication.feed.synd.SyndContent;
import com.sun.syndication.feed.synd.SyndContentImpl;
import com.sun.syndication.feed.synd.SyndEntry;
import com.sun.syndication.feed.synd.SyndEntryImpl;
import com.sun.syndication.feed.synd.SyndFeed;
import com.sun.syndication.feed.synd.SyndFeedImpl;
import org.apache.commons.lang.BooleanUtils;
import org.apache.commons.lang.StringUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.Collection;
import java.util.Collections;
import java.util.Comparator;
import java.util.HashMap;
import java.util.HashSet;
import java.util.LinkedHashSet;
import java.util.List;
import java.util.Map;
import java.util.Set;

public class DefaultBookmarkUtils implements BookmarkUtils
{
	private static final Logger LOG = LoggerFactory.getLogger(BookmarkUtils.class);

    private static final Collection<String> SUPPORTED_CONTENT_TYPES = Arrays.asList(
            Draft.CONTENT_TYPE,
            Page.CONTENT_TYPE,
            BlogPost.CONTENT_TYPE,
            Comment.CONTENT_TYPE
    );

    private static final Collection<String> SUPPORTED_PROTOCOLS = Arrays.asList(
            "HTTP",
            "HTTPS",
            "FILE"
    );

	private PageManager pageManager;

	private ContentPropertyManager contentPropertyManager;

	private PermissionManager permissionManager;

	private LabelManager labelManager;

	private SpaceManager spaceManager;

	private I18NBeanFactory i18NBeanFactory;

	private LocaleManager localeManager;

	private BookmarkMacroParser bookmarkMacroParser;

	private SettingsManager settingsManager;

    private UserAccessor userAccessor;

    private VelocityHelperService velocityHelperService;

    private XhtmlContent xhtmlContent;

    public DefaultBookmarkUtils(PageManager pageManager, ContentPropertyManager contentPropertyManager, PermissionManager permissionManager, LabelManager labelManager, SpaceManager spaceManager, I18NBeanFactory i18NBeanFactory, LocaleManager localeManager, BookmarkMacroParser bookmarkMacroParser, SettingsManager settingsManager, UserAccessor userAccessor, VelocityHelperService velocityHelperService, XhtmlContent xhtmlContent)
    {
        setPageManager(pageManager);
        setContentPropertyManager(contentPropertyManager);
        setPermissionManager(permissionManager);
        setLabelManager(labelManager);
        setSpaceManager(spaceManager);
        setI18NBeanFactory(i18NBeanFactory);
        setLocaleManager(localeManager);
        setBookmarkMacroParser(bookmarkMacroParser);
        setSettingsManager(settingsManager);
        setUserAccessor(userAccessor);
        setVelocityHelperService(velocityHelperService);
        setXhtmlContent(xhtmlContent);
    }

    @SuppressWarnings("unused")
    public DefaultBookmarkUtils()
    {
        this(null, null, null, null, null, null, null, null, null, null, null, null);
    }

    public void setPageManager(PageManager pageManager)
    {
        this.pageManager = pageManager;
    }

    public void setContentPropertyManager(ContentPropertyManager contentPropertyManager)
    {
        this.contentPropertyManager = contentPropertyManager;
    }

    public void setPermissionManager(PermissionManager permissionManager)
    {
        this.permissionManager = permissionManager;
    }

    public void setLabelManager(LabelManager labelManager)
    {
        this.labelManager = labelManager;
    }

    public void setSpaceManager(SpaceManager spaceManager)
    {
        this.spaceManager = spaceManager;
    }

    public void setI18NBeanFactory(I18NBeanFactory i18NBeanFactory)
    {
        this.i18NBeanFactory = i18NBeanFactory;
    }

    public void setLocaleManager(LocaleManager localeManager)
    {
        this.localeManager = localeManager;
    }

    public void setBookmarkMacroParser(BookmarkMacroParser bookmarkMacroParser)
    {
        this.bookmarkMacroParser = bookmarkMacroParser;
    }

    public void setSettingsManager(SettingsManager settingsManager)
    {
        this.settingsManager = settingsManager;
    }

    public void setUserAccessor(UserAccessor userAccessor)
    {
        this.userAccessor = userAccessor;
    }

    public void setVelocityHelperService(VelocityHelperService velocityHelperService)
    {
        this.velocityHelperService = velocityHelperService;
    }

    public void setXhtmlContent(XhtmlContent xhtmlContent)
    {
        this.xhtmlContent = xhtmlContent;
    }

    public Bookmark getBookmark(long contentId)
    {
        ContentEntityObject contentEntityObject = pageManager.getById(contentId);
        return null == contentEntityObject
                ? null
                : getBookmark(contentEntityObject, AuthenticatedUserThreadLocal.getUser());
	}

    public Page getBookmarkParent(Space space, boolean create)
    {
        Page parentPage = null;
        if (space != null)
        {
            parentPage = pageManager.getPage(space.getKey(), BOOKMARK_PARENT_PAGE);

            if (parentPage == null && create)
            {
                if (LOG.isDebugEnabled())
                    LOG.debug("bookmark parent page doesn't exist, creating in space: " + space.getKey());

                parentPage = new Page();
                parentPage.setSpace(space);
                parentPage.setTitle(BOOKMARK_PARENT_PAGE);
                parentPage.setBodyAsString(getBookmarkParentPageContent(space, parentPage)) ;

                pageManager.saveContentEntity(parentPage, null);

                if (LOG.isDebugEnabled())
                    LOG.debug("Create " + BOOKMARK_PARENT_PAGE + " for space " + space.getKey());
            }
        }

        return parentPage;
    }

    private String getBookmarkParentPageContent(Space space, Page parentPage)
    {
        List<RuntimeException> conversionErrors = new ArrayList<RuntimeException>();
        Map<String, Object> velocityContext = velocityHelperService.createDefaultVelocityContext();

        velocityContext.put("spaceName", space.getName());
        velocityContext.put("spaceKey", space.getKey());

        String wikiMarkup = velocityHelperService.getRenderedTemplate("templates/plugins/socialbookmarking/bookmarkparentpage.vm", velocityContext);
        String storageXhtml = xhtmlContent.convertWikiToStorage(wikiMarkup, new DefaultConversionContext(parentPage.toPageContext()), conversionErrors);
        if (!conversionErrors.isEmpty())
        {
            LOG.error(String.format("Unable to convert parent page wiki markup below to XHTML storage:\n%s", wikiMarkup));
            for (RuntimeException conversionError : conversionErrors)
                LOG.error("Conversion error", conversionError);
        }

        return storageXhtml;
    }

	public Bookmarks getBookmarkList(Set<String> spaceKeys, String labels, String creators, String sortParams, int max,
			int pageNumber, boolean reverse)
    {

		Bookmarks bookmarks = getBookmarkList(spaceKeys, labels, creators, sortParams, max, pageNumber);

		if (reverse) {
			Collections.reverse(bookmarks.getBookmarkList());
		}
		return bookmarks;
	}

    public Bookmarks getBookmarkList(Set<String> spaceKeys, String labelsString, String creators, String sortParams, int max,
    		int pageNumber) {
        if (LOG.isDebugEnabled())
            LOG.debug("Getting a bookmarks list from spaces " + spaceKeys + ", labels " + labelsString + ", creators " + creators + ", sorted in " + sortParams + " and limited to " + max);


        Bookmarks bookmarks = new Bookmarks();
        User user = AuthenticatedUserThreadLocal.getUser();

        Comparator comparator = createComparator(sortParams);
        if (comparator == null)
        {
            // defalut search by date
            comparator = new BookmarkDateComparator();
        }

        List<Space> spaces = getSpaces(spaceKeys);
        List<Label> labels = getLabels(labelsString);
        Set<String> creatorNames = getUserNames(creators);

        Set<ContentEntityObject> finalBookmarkPages = new HashSet<ContentEntityObject>();
        for (Space space : spaces)
        {
            if (space != null)
            {
                Page bookmarksPage = getBookmarkParent(space, false);
                if (bookmarksPage != null)
                {
                    Set<ContentEntityObject> bookmarkPages = new HashSet<ContentEntityObject>(bookmarksPage.getChildren());

                    if (!labels.isEmpty())
                    {
                        Set<Labelable> bookmarkLabelSet = new HashSet<Labelable>();
                        for (Label label : labels)
                            bookmarkLabelSet.addAll(
                                    Collections2.filter(
                                            labelManager.getCurrentContentForLabelAndSpace(label, space.getKey()),
                                            Predicates.instanceOf(ContentEntityObject.class)
                                    )
                            );

                        if (bookmarkLabelSet.isEmpty())
                        {
                            bookmarkPages.clear();
                        }
                        else
                        {
                            // Create the intersection between the label page set and the actual bookmarks in the space.
                            bookmarkPages.retainAll(bookmarkLabelSet);
                        }
                    }


                    // If there are creator users specified, check the page creator before adding to the main bookmark collection.
                    if (creatorNames.isEmpty())
                    {
                        finalBookmarkPages.addAll(bookmarkPages);
                    }
                    else
                    {
                        for (ContentEntityObject bookmarkPage : bookmarkPages)
                        {
                            if (StringUtils.equals(Page.CONTENT_TYPE, bookmarkPage.getType()))
                                if (creatorNames.contains(StringUtils.lowerCase(bookmarkPage.getCreatorName())))
                                    finalBookmarkPages.add(bookmarkPage);
                        }
                    }
                }
            }
        }

        // Convert the list of bookmark pages into bookmark objects
        // -> this could probably be moved into the main loop to be more
        // efficient
        // -> and use a tree set so we don't have to sort again as well....
        List<Bookmark> bookmarkList = getViewableBookmarks(finalBookmarkPages, user);
        bookmarks.setHasNextPage(hasNextPage(bookmarkList.size(), max, pageNumber));
        Collections.sort(bookmarkList, comparator);

        if (pageNumber > 1) // calculate start & end item
        {
        	int startItem = (pageNumber-1) * max;
        	int endItem = startItem + max;
        	if (endItem > bookmarkList.size())
        	{
        		endItem = bookmarkList.size();
        	}
        	bookmarks.setBookmarkList(bookmarkList.subList(startItem, endItem));
        }
        else
        {
	        if (max >= 0 && bookmarkList.size() > max)
	        {
	        	bookmarks.setBookmarkList(bookmarkList.subList(0, max));
	        }
	        else
	        {
	        	bookmarks.setBookmarkList(bookmarkList);
	        }
        }
        return bookmarks;
    }

    private boolean hasNextPage(int totalItem, int max, int pageNumber)
    {
    	return (totalItem > max * pageNumber);
    }

	/**
	 * Creates an AbstractBookmarkComparator based on the given sortParams input
	 * string.
	 *
	 * @param sortParams
	 * @return
	 */
	protected AbstractBookmarkComparator createComparator(String sortParams) {

		AbstractBookmarkComparator bookmarkComparator;
		if (sortParams != null) {

			String[] splitParams = sortParams.split("[,\\s]+", 2);

			if (SORT_CREATION.equalsIgnoreCase(splitParams[0])) {
				bookmarkComparator = new BookmarkDateComparator();
			} else if (SORT_CREATOR.equalsIgnoreCase(splitParams[0])) {
				bookmarkComparator = new BookmarkAuthorComparator();
			} else if (SORT_TITLE.equalsIgnoreCase(splitParams[0])) {
				bookmarkComparator = new BookmarkTitleComparator();
			} else {
				// Unkown sort param, return null
				return null;
			}

			// If there are more search params, add them to the comparator
			if (splitParams.length > 1 && splitParams[1] != null) {
				bookmarkComparator.setNextComparator(createComparator(splitParams[1]));
			}

			return bookmarkComparator;

		} else {
			return null;
		}
	}

    private List<Space> getSpaces(Set<String> spaceKeys)
    {
        Set<Space> spaces = new LinkedHashSet<Space>();

        for (String spaceKey : spaceKeys)
        {
            if (StringUtils.isBlank(spaceKey) || spaceKey.equals(SPACES_ALL))
            {
                spaces.addAll(spaceManager.getAllSpaces());
                break;
            }
            else if (spaceKey.equals(SPACES_GLOBAL))
            {
                spaces.addAll(spaceManager.getAllSpaces(SpacesQuery.newQuery().withSpaceType(SpaceType.GLOBAL).build()));
            }
            else if (spaceKey.equals(SPACES_PERSONAL))
            {
                spaces.addAll(spaceManager.getAllSpaces(SpacesQuery.newQuery().withSpaceType(SpaceType.PERSONAL).build()));
            }
            else
            {
                Space space = spaceManager.getSpace(StringUtils.trim(spaceKey));
                if (null != space)
                    spaces.add(space);
            }
        }

        return new ArrayList<Space>(spaces);
    }

    private List<Label> getLabels(String labelsString)
    {
        return new ArrayList<Label>(Collections2.filter(
                Collections2.transform(
                        Arrays.asList(StringUtils.split(StringUtils.defaultString(labelsString), ", ")),
                        new Function<String, Label>()
                        {
                            public Label apply(String label)
                            {
                                final Label labelObject = labelManager.getLabel(label);
                                if (null == labelObject && label.startsWith(FORLABEL_PREFIX))
                                {
                                    // The "for" labels sometimes aren't found if the username has
                                    // special characters, in this case just create a new label.
                                    return new Label(label);
                                }
                                return labelObject;
                            }
                        }
                ),
                Predicates.<Object>notNull()
        ));
    }

    private Set<String> getUserNames(String creators)
    {
        return new HashSet<String>(
                Arrays.asList(
                        StringUtils.split(StringUtils.defaultString(StringUtils.lowerCase(creators)), ", ")
                )
        );
    }

    List<Bookmark> getViewableBookmarks(Set<? extends ContentEntityObject> bookmarkContentEntityObjects, User user)
    {
        List<Bookmark> viewableBookmarks = new ArrayList<Bookmark>();
        for (ContentEntityObject bookmarkContentEntityObject : bookmarkContentEntityObjects)
        {
            Bookmark bookmark = getBookmark(bookmarkContentEntityObject, user);
            if (bookmark != null)
                viewableBookmarks.add(bookmark);
        }
        return viewableBookmarks;
    }
	
	public String createRssUrl(Space anchorSpace, String spaceKeys, String labels, String sortParams, int max, boolean reverse, Map<?, ?> displayParams) {
        List<String> parameterPairs = new ArrayList<String>();
        StringBuilder paramBuilder = new StringBuilder();

        parameterPairs.add(paramBuilder.append("spaceKey=").append(GeneralUtil.urlEncode(anchorSpace.getKey())).toString());

		// Space Keys
		if (StringUtils.isNotBlank(spaceKeys))
        {
            paramBuilder.setLength(0);
			parameterPairs.add(paramBuilder.append("spaceKeys=").append(GeneralUtil.urlEncode(spaceKeys)).toString());
        }

		// Labels
		if (StringUtils.isNotBlank(labels))
        {
            paramBuilder.setLength(0);
			parameterPairs.add(paramBuilder.append("labels=").append(GeneralUtil.urlEncode(labels)).toString());
        }

		// Sort Params
		if (StringUtils.isNotBlank(sortParams))
        {
            paramBuilder.setLength(0);
			parameterPairs.add(paramBuilder.append("sort=").append(GeneralUtil.urlEncode(sortParams)).toString());
        }

		// Max
        paramBuilder.setLength(0);
		parameterPairs.add(paramBuilder.append("max=").append(max).toString());

		// Reverse
		if (reverse)
			parameterPairs.add("reverse=true");

		// Display Params
		if (displayParams != null)
        {
			for (Map.Entry entry : displayParams.entrySet())
            {
				if (null != entry.getValue() && Boolean.FALSE.equals(entry.getValue()))
                {
                    paramBuilder.setLength(0);
					parameterPairs.add(paramBuilder.append(entry.getKey()).append('=').append(false).toString());
                }
            }
        }

        parameterPairs.add("output=rss");

        return new StringBuilder("/plugins/socialbookmarking/bookmarks.action?").append(StringUtils.join(parameterPairs, "&")).toString();
	}


    public SyndFeed createSyndFeed(List<Bookmark> bookmarks, ThemeHelper helper, Map<String, String> params)
    {
        // setup velocity context for creating html description
        Map<String, Object> velocityContext = velocityHelperService.createDefaultVelocityContext();
        Map<String, Object> parameters = new HashMap<String, Object>();
        if (null != params)
            parameters.putAll(params);

        AbstractBookmarksMacro.getParams(parameters, velocityContext);

        // Override the title parameter
        velocityContext.put(AbstractBookmarksMacro.SHOWTITLE_PARAM, Boolean.FALSE);

        // Set the base url so the template can make links in the rss feed absolute links
        velocityContext.put("baseUrl", settingsManager.getGlobalSettings().getBaseUrl());
        velocityContext.put("helper", new PageHelper());
        velocityContext.put("generalUtil", new GeneralUtil());

        SyndFeed feed = getSyndFeed();
        List<SyndEntry> entries = new ArrayList<SyndEntry>();

        for (Bookmark bookmark : bookmarks)
        {
            // Add bookmark specific items to the velocity context
            velocityContext.put("bookmark", bookmark);

            SyndContent description = new SyndContentImpl();
            description.setType("html");
            description.setValue(getViewBookmarkHtml(velocityContext));

            SyndEntry entry = new SyndEntryImpl();
            entry.setTitle(bookmark.getTitle());
            entry.setLink(bookmark.getUrl());
            entry.setPublishedDate(bookmark.getAddedDate());
            entry.setAuthor((bookmark.getCreator() != null) ? bookmark.getCreator().getFullName() : UserAccessor.ANONYMOUS);
            entry.setDescription(description);

            List<SyndCategory> categories = new ArrayList<SyndCategory>();
            for (Label label : bookmark.getLabels())
            {
                SyndCategory category = new SyndCategoryImpl();
                category.setName(label.toString());
                categories.add(category);
            }
            entry.setCategories(categories);

            entries.add(entry);
        }

        feed.setEntries(entries);

        return feed;
    }

    protected String getViewBookmarkHtml(Map velocityContext)
    {
        return velocityHelperService.getRenderedTemplate(getViewBookmarkTemplatePath(), velocityContext);
    }

    protected String getViewBookmarkTemplatePath()
    {
        return "templates/plugins/socialbookmarking/viewbookmark.vm";
    }


    private I18NBean getI18NBean()
    {
        return i18NBeanFactory.getI18NBean(localeManager.getLocale(AuthenticatedUserThreadLocal.getUser()));
    }

    private SyndFeed getSyndFeed()
    {
        // Create i18n bean
        I18NBean i18nbean = getI18NBean();

        SyndFeed feed = new SyndFeedImpl();
        feed.setTitle(i18nbean.getText("bookmark.rss.title"));
        feed.setLink(settingsManager.getGlobalSettings().getBaseUrl());
        feed.setUri(settingsManager.getGlobalSettings().getBaseUrl());
        feed.setDescription(i18nbean.getText("bookmark.rss.description"));

        return feed;
    }


    /**
	 * Returns a {@link Bookmark} object from a {@link ContentEntityObject}
	 *
	 * @param contentEntityObject
     * The {@link ContentEntityObject} to interpret as a bookmark
	 * @param user
     * The {@link User} to which the interpretation is done for
	 * @return
     * A {@link Bookmark} object presenting the interpretation, or null if {@code contentEntityObject} is not a bookmark
	 */
    public Bookmark getBookmark(ContentEntityObject contentEntityObject, User user)
    {
        return permissionManager.hasPermission(user, Permission.VIEW, contentEntityObject)
                && BooleanUtils.toBoolean(contentPropertyManager.getStringProperty(contentEntityObject, ISBOOKMARK_PROPERTY_KEY))
                ? convertPageToBookmark(contentEntityObject, user)
                : null;
    }

    public Bookmark convertPageToBookmark(ContentEntityObject contentEntityObject, User user)
    {
        Bookmark bookmark = new Bookmark();

        if (!isSupportedContentType(contentEntityObject))
            throw new IllegalArgumentException("Content " + contentEntityObject + " of type " + contentEntityObject.getType() + " not supported");

        bookmark.setId(contentEntityObject.getIdAsString());
        bookmark.setTitle(contentEntityObject.getTitle());
        bookmark.setUrl(contentPropertyManager.getTextProperty(contentEntityObject, URL_PROPERTY_KEY));
        bookmark.setDescription(getBookmarkDescriptionRendered(contentEntityObject));
        bookmark.setLabels(contentEntityObject.getVisibleLabels(user));
        bookmark.setAddedDate(contentEntityObject.getCreationDate());
        bookmark.setCreator(userAccessor.getUser(contentEntityObject.getCreatorName()));
        bookmark.setCommentCount(contentEntityObject.getComments().size());

        Space bookmarkSpace = getSpaceFromContentEntity(contentEntityObject);
        bookmark.setSpaceName(bookmarkSpace.getName());
        bookmark.setSpaceUrlPath(bookmarkSpace.getUrlPath());

        // Only ever allow bookmark edit or removal if {bookmark} is used on a page or blogpost
        if (contentEntityObject instanceof Page)
        {
            bookmark.setEditPermission(permissionManager.hasPermission(user, Permission.EDIT, contentEntityObject));
            bookmark.setRemovePermission(permissionManager.hasPermission(user, Permission.REMOVE, contentEntityObject));
        }

        return bookmark;
    }

    public Space getSpaceFromContentEntity(ContentEntityObject contentEntityObject)
    {
        if (contentEntityObject instanceof AbstractPage)
            return ((AbstractPage) contentEntityObject).getSpace();
        if (contentEntityObject instanceof Comment)
            return getSpaceFromContentEntity(((Comment) contentEntityObject).getOwner());

        return spaceManager.getSpace(((Draft) contentEntityObject).getDraftSpaceKey());
    }

    private String getBookmarkDescriptionRendered(ContentEntityObject contentEntityObject)
    {
        String bookmarkDescription = StringUtils.defaultString(bookmarkMacroParser.getParameters(contentEntityObject).get("description"));
        try
        {
            return xhtmlContent.convertStorageToView(
                    bookmarkDescription,
                    new DefaultConversionContext(contentEntityObject.toPageContext())
            );
        }
        catch (Exception e)
        {
            LOG.error("Error rendering bookmark description. Returning markup instead", e);
            return bookmarkDescription;
        }
    }

    public boolean isSupportedContentType(ContentEntityObject contentEntityObject)
    {
        return null != contentEntityObject && SUPPORTED_CONTENT_TYPES.contains(contentEntityObject.getType());
    }

    public boolean isValidProtocol(String url) {
        String trimmedUrl = url.trim();
        for (String s : SUPPORTED_PROTOCOLS) {
            if (StringUtils.startsWithIgnoreCase(trimmedUrl, s))
            {
                return true;
            }
        }
        return false;
    }

}
