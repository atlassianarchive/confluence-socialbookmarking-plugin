package com.atlassian.confluence.plugins.socialbookmarking.macros;

import com.atlassian.confluence.content.render.xhtml.ConversionContext;
import com.atlassian.confluence.content.render.xhtml.DefaultConversionContext;
import com.atlassian.confluence.macro.Macro;
import com.atlassian.confluence.setup.settings.SettingsManager;
import com.atlassian.confluence.util.GeneralUtil;
import com.atlassian.confluence.util.i18n.I18NBean;
import com.atlassian.confluence.util.i18n.I18NBeanFactory;
import com.atlassian.renderer.RenderContext;
import com.atlassian.renderer.TokenType;
import com.atlassian.renderer.v2.RenderMode;
import com.atlassian.renderer.v2.RenderUtils;
import com.atlassian.renderer.v2.macro.BaseMacro;
import org.apache.commons.lang.StringUtils;

import java.util.Map;

/**
 * A macro that generates the links in the information panel in the
 * &quot;.bookmarks&quot; page
 */
public class SpaceBookmarksLink extends BaseMacro implements Macro
{
    private SettingsManager settingsManager;

    private I18NBeanFactory i18NBeanFactory;

    public SpaceBookmarksLink(SettingsManager settingsManager, I18NBeanFactory i18NBeanFactory)
    {
        setSettingsManager(settingsManager);
        setI18NBeanFactory(i18NBeanFactory);
    }

    @SuppressWarnings("unused")
    public SpaceBookmarksLink()
    {
        this(null, null);
    }

    public void setSettingsManager(SettingsManager settingsManager)
    {
        this.settingsManager = settingsManager;
    }

    public void setI18NBeanFactory(I18NBeanFactory i18NBeanFactory)
    {
        this.i18NBeanFactory = i18NBeanFactory;
    }

    private I18NBean getI18NBean()
    {
        return i18NBeanFactory.getI18NBean();
    }

    private String getText(String i18nKey)
    {
        return getI18NBean().getText(i18nKey);
    }

    @Override
    public TokenType getTokenType(Map parameters, String body, RenderContext context)
    {
        return TokenType.INLINE;
    }

    public boolean hasBody()
    {
        return true;
    }

    public RenderMode getBodyRenderMode()
    {
        return RenderMode.NO_RENDER;
    }

    public String execute(Map<String, String> macroParams, String body, ConversionContext conversionContext)
    {
        String urlPath = macroParams.get("urlPath");

        if (StringUtils.isBlank(urlPath))
        {
            return RenderUtils.error(
                    getText("dotbookmarkslink.error.urlpathnotspecified")
            );
        }
        else
        {
            StringBuilder stringBuilder = new StringBuilder();
            String url = stringBuilder.append(settingsManager.getGlobalSettings().getBaseUrl()).append(urlPath).toString();

            stringBuilder.setLength(0);

            return stringBuilder
                    .append("<a href=\"")
                    .append(GeneralUtil.htmlEncode(url))
                    .append("\">")
                    .append(GeneralUtil.htmlEncode(StringUtils.defaultString(macroParams.get("alias"), url)))
                            .append("</a>")
                            .toString();
        }
    }

    public String execute(Map parameters, String body, RenderContext renderContext) 
    {
        return execute(parameters, body, new DefaultConversionContext(renderContext));
    }

    public BodyType getBodyType()
    {
        return BodyType.NONE;
    }

    public OutputType getOutputType()
    {
        return OutputType.INLINE;
    }
}
