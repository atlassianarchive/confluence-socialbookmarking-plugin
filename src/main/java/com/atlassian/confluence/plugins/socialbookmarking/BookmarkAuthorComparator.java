package com.atlassian.confluence.plugins.socialbookmarking;

import com.atlassian.confluence.user.UserAccessor;
import com.atlassian.user.User;
import org.apache.commons.lang.StringUtils;

public class BookmarkAuthorComparator extends AbstractBookmarkComparator
{
    private String getCreatorFullName(Bookmark bookmark)
    {
        User creator = bookmark.getCreator();
        return null == creator ? UserAccessor.ANONYMOUS : StringUtils.defaultString(creator.getFullName());
    }

    @Override
    protected int compareInternal(Bookmark leftBookmark, Bookmark rightBookmark)
    {
        return getCreatorFullName(leftBookmark).compareTo(getCreatorFullName(rightBookmark));
    }
}