package com.atlassian.confluence.plugins.socialbookmarking.macros;

import com.atlassian.confluence.content.render.xhtml.ConversionContext;
import com.atlassian.confluence.content.render.xhtml.HtmlToXmlConverter;
import com.atlassian.confluence.content.render.xhtml.definition.RichTextMacroBody;
import com.atlassian.confluence.macro.xhtml.MacroMigration;
import com.atlassian.confluence.xhtml.api.MacroDefinition;
import com.atlassian.confluence.xhtml.api.XhtmlContent;
import org.apache.commons.lang.BooleanUtils;
import org.apache.commons.lang.StringUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.util.ArrayList;
import java.util.List;

/**
 * Migrates wiki markup in the bookmark macro to plain text
 */
public class BookmarkMacroMigrator implements MacroMigration
{
    private static final Logger LOG = LoggerFactory.getLogger(BookmarkMacroMigrator.class);

    private final XhtmlContent xhtmlContent;

    private final HtmlToXmlConverter htmlToXmlConverter;

    public BookmarkMacroMigrator(XhtmlContent xhtmlContent, HtmlToXmlConverter htmlToXmlConverter)
    {
        this.xhtmlContent = xhtmlContent;
        this.htmlToXmlConverter = htmlToXmlConverter;
    }

    public MacroDefinition migrate(MacroDefinition macroDefinition, ConversionContext conversionContext)
    {
        String bodyText = StringUtils.defaultString(macroDefinition.getBodyText());
        List<RuntimeException> conversionErrors = new ArrayList<RuntimeException>();

        try
        {
            String xhtml = BooleanUtils.toBoolean(macroDefinition.getParameters().get("htmlbody")) // Parameter 'htmlbody' is passed via UpdateBookmarkAction
                    ? htmlToXmlConverter.convert(bodyText)
                    : xhtmlContent.convertWikiToStorage(bodyText, conversionContext, conversionErrors);

            if (conversionErrors.isEmpty())
            {
                // Only change macro definition if there isn't any error in the conversion
                macroDefinition.setBody(new RichTextMacroBody(xhtml));
            }
            else
            {
                LOG.error(String.format("Error(s) encountered when converting wik mark below to storage:\n%s", bodyText));
                for (Exception conversionError : conversionErrors)
                    LOG.error("Conversion error", conversionError);
            }
        }
        catch (Exception e)
        {
            LOG.error(String.format("Unable to migrate wiki markup in bookmark macro:\n%s", bodyText), e);
        }

        return macroDefinition;
    }
}
