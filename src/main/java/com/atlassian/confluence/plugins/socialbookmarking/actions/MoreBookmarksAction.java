package com.atlassian.confluence.plugins.socialbookmarking.actions;

import com.atlassian.confluence.core.ConfluenceActionSupport;
import com.atlassian.confluence.plugins.socialbookmarking.Bookmark;
import com.atlassian.confluence.plugins.socialbookmarking.BookmarkUtils;
import com.atlassian.confluence.plugins.socialbookmarking.Bookmarks;
import org.apache.commons.lang.StringUtils;

import java.util.LinkedHashSet;
import java.util.List;
import java.util.Set;

public class MoreBookmarksAction extends ConfluenceActionSupport 
{
	private String spaces;
	private String labels;
	private String creators;
	private String sort;
	private int max;
	private int pageNumber;
	private boolean showTitle;
	private boolean showEditLinks;
	private boolean showDescription;
	private boolean showDate;
	private boolean showSpace;
	private boolean showAuthor;
	private boolean showViewLink;
	private boolean showLabels;
	private boolean showComments;
	private boolean nextPage;
	private List<Bookmark> bookmarks; 
	private BookmarkUtils bookmarkUtils;
	
	public void setBookmarkUtils(BookmarkUtils bookmarkUtils)
	{
		this.bookmarkUtils = bookmarkUtils;
	}
	
	@Override
    public String execute() throws Exception
    {
		Bookmarks bookmarkList = bookmarkUtils.getBookmarkList(parseSpaceKeys(spaces, ","), null, null, null, 0 == max ? Integer.MAX_VALUE : max, pageNumber, false);
		bookmarks = bookmarkList.getBookmarkList();
		setNextPage(bookmarkList.isHasNextPage());
		return SUCCESS;
    }

	public void setSpaces(String spaces) {
		this.spaces = spaces;
	}

	public String getSpaces() {
		return spaces;
	}

	public void setBookmarks(List<Bookmark> bookmarks) {
		this.bookmarks = bookmarks;
	}

	public List<Bookmark> getBookmarks() {
		return bookmarks;
	}
	
	public String getLabels() {
		return labels;
	}

	public void setLabels(String labels) {
		this.labels = labels;
	}

	public String getCreators() {
		return creators;
	}

	public void setCreators(String creators) {
		this.creators = creators;
	}

	public String getSort() {
		return sort;
	}

	public void setSort(String sort) {
		this.sort = sort;
	}
	
	public void setMax(int max) {
		this.max = max;
	}

	public int getMax() {
		return max;
	}

	public void setPageNumber(int pageNumber) {
		this.pageNumber = pageNumber;
	}

	public int getPageNumber() {
		return pageNumber;
	}

	public boolean isShowTitle() {
		return showTitle;
	}

	public void setShowTitle(boolean showTitle) {
		this.showTitle = showTitle;
	}

	public boolean isShowEditLinks() {
		return showEditLinks;
	}

	public void setShowEditLinks(boolean showEditLinks) {
		this.showEditLinks = showEditLinks;
	}

	public boolean isShowDescription() {
		return showDescription;
	}

	public void setShowDescription(boolean showDescription) {
		this.showDescription = showDescription;
	}

	public boolean isShowDate() {
		return showDate;
	}

	public void setShowDate(boolean showDate) {
		this.showDate = showDate;
	}

	public boolean isShowSpace() {
		return showSpace;
	}

	public void setShowSpace(boolean showSpace) {
		this.showSpace = showSpace;
	}

	public boolean isShowAuthor() {
		return showAuthor;
	}

	public void setShowAuthor(boolean showAuthor) {
		this.showAuthor = showAuthor;
	}

	public boolean isShowViewLink() {
		return showViewLink;
	}

	public void setShowViewLink(boolean showViewLink) {
		this.showViewLink = showViewLink;
	}

	public boolean isShowLabels() {
		return showLabels;
	}

	public void setShowLabels(boolean showLabels) {
		this.showLabels = showLabels;
	}

	public boolean isShowComments() {
		return showComments;
	}

	public void setShowComments(boolean showComments) {
		this.showComments = showComments;
	}

	public BookmarkUtils getBookmarkUtils() {
		return bookmarkUtils;
	}
	
	public void setNextPage(boolean nextPage) {
		this.nextPage = nextPage;
	}

	public boolean isNextPage() {
		return nextPage;
	}
	
	private Set<String> parseSpaceKeys(String spaceKey, String separators)
	{
		String[] spaceKeysArray = StringUtils.split(spaceKey, separators);
		Set<String> spaceKeys = new LinkedHashSet<String>(spaceKeysArray.length);
		
		for (String aSpaceKey : spaceKeysArray)
			spaceKeys.add(StringUtils.trim(aSpaceKey));
		
		return spaceKeys;
	}
}
