package com.atlassian.confluence.plugins.socialbookmarking.extractor;

import com.atlassian.bonnie.Searchable;
import com.atlassian.bonnie.search.Extractor;
import com.atlassian.confluence.core.ContentPropertyManager;
import com.atlassian.confluence.pages.AbstractPage;
import com.atlassian.confluence.plugins.socialbookmarking.BookmarkUtils;
import org.apache.lucene.document.Document;
import org.apache.lucene.document.Field;
import org.apache.lucene.document.TextField;

public class BookmarkExtractor implements Extractor
{
    private ContentPropertyManager contentPropertyManager;

    public void setContentPropertyManager(ContentPropertyManager contentPropertyManager)
    {
        this.contentPropertyManager = contentPropertyManager;
    }

    public void addFields(
            final Document document,
            final StringBuffer defaultSearchableText,
            final Searchable searchableContent)
    {

        if (searchableContent instanceof AbstractPage)
        {
            final AbstractPage abstractPage = (AbstractPage) searchableContent;
            final boolean isBookmark = Boolean.valueOf(
                    contentPropertyManager.getStringProperty(
                            abstractPage, BookmarkUtils.ISBOOKMARK_PROPERTY_KEY)).booleanValue();

            if (isBookmark)
                document.add(new TextField("bookmark", "yes true", Field.Store.NO));
        }
    }
}
