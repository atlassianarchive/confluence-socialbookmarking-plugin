package com.atlassian.confluence.plugins.socialbookmarking;


import java.util.Comparator;

public abstract class AbstractBookmarkComparator implements Comparator<Bookmark>
{
    private AbstractBookmarkComparator nextComparator;

    public final int compare(Bookmark leftBookmark, Bookmark rightBookmark)
    {
        int retValue = compareInternal(leftBookmark, rightBookmark);
        AbstractBookmarkComparator nextComparator = getNextComparator();
        return retValue == 0 && nextComparator != null
                ? nextComparator.compare(leftBookmark, rightBookmark)
                : retValue;
    }

    /**
     * Implementations should compare the specified bookmarks here as specified in
     * {@link Comparator#compare(Object, Object)}
     * @param leftBookmark
     * One bookmark to compare. Can be {@code null}.
     * @param rightBookmark
     * The other to compare. Can be {@code null}.
     *
     * @return
     * See {@link Comparator#compare(Object, Object)}
     */
    protected abstract int compareInternal(Bookmark leftBookmark, Bookmark rightBookmark);

    public AbstractBookmarkComparator getNextComparator()
    {
        return nextComparator;
    }

    public void setNextComparator(AbstractBookmarkComparator nextComparator)
    {
        this.nextComparator = nextComparator;
    }
}
