package com.atlassian.confluence.plugins.socialbookmarking;

import com.atlassian.confluence.core.ContentEntityObject;

import java.util.Map;

public interface BookmarkMacroParser {

    Map<String, String> getParameters(ContentEntityObject contentEntityObject);
}
