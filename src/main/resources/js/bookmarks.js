jQuery(function($) {
	var confluenceContextPath = $("#confluence-context-path").attr("content");
	$(".more-bookmarks").each(function() {
		var $moreBookmarks = $(this);
		var moreBookmarksData = {};
		$("fieldset input[class='param']", $moreBookmarks).each(function() {
			moreBookmarksData[this.name] = this.value;
		});
		
		$(".viewMoreBookmarksLink", $moreBookmarks).click(function(){
			var $moreBookmarkLink = $(this);
			if ($moreBookmarkLink.data("pageNumber")) {
				var pn = $moreBookmarkLink.data("pageNumber");
				pn += 1;
				$moreBookmarkLink.data("pageNumber", pn);
			} else {
				$moreBookmarkLink.data("pageNumber", 2);
			};
			moreBookmarksData["pageNumber"] = $moreBookmarkLink.data("pageNumber");
			$moreBookmarks.data("moreBookmarksCacheData", moreBookmarksData);
			
			var waitingImage = $(".waiting-image", $moreBookmarks).removeClass("hidden");
			var mostRecentWithoutLabels = $("fieldset input[name='mostRecentWithoutLabels']", $moreBookmarks).val();
			var mostRecentWithLabels = $("fieldset input[name='mostRecentWithLabels']", $moreBookmarks).val();
			
			$.ajax({
				cache: false,
				data : $moreBookmarks.data("moreBookmarksCacheData"),
                dataType : "html", 
                success : function(serverGeneratedHtml) {
                    var $moreBookmarksContainer = $(".more-bookmarks-container", $moreBookmarks).append(serverGeneratedHtml);
                    if ($(".lastPage", $moreBookmarksContainer).length) {
                    		$moreBookmarkLink.remove();
                    }
                    waitingImage.addClass("hidden");
                    
                    var max = $("fieldset input[name='max']", $moreBookmarks).val();
                    var initialBookmarksCount = parseInt($("fieldset input[name='initialBookmarksCount']", $moreBookmarks).val());
                    var bookmarksCount = initialBookmarksCount + $(".bookmarkContainer", $moreBookmarks).length;
                    
                    if (max) {
                    	var spaceName = $("fieldset input[name='spaceName']", $moreBookmarks).val();
            			var labelString = $("fieldset input[name='labelString']", $moreBookmarks).val();
            			var $bookmarksHeading = $(".bookmarkListHeading", $moreBookmarks.closest(".bookmarkListContainer").prev());
            			
            			if (labelString) {
            				$bookmarksHeading.text(
            					AJS.format(
            						$("fieldset input[name='mostRecentWithLabels']", $moreBookmarks).val(),
            						bookmarksCount,
            				        spaceName,
            				        labelString
            					)
            				);
            			}
            			else
            			{
            				$bookmarksHeading.text(
                				AJS.format(
                					$("fieldset input[name='mostRecentWithoutLabels']", $moreBookmarks).val(),
                					bookmarksCount,
                				    spaceName
                				)
                			);
            			}
                    }
                },
                error : function(XMLHttpRequest, textStatus, errorThrown) {
                	$(".moreBookmarksError", $moreBookmarks).html(XMLHttpRequest.responseText);
                	$moreBookmarkLink.remove();
                	waitingImage.addClass("hidden");
                }, 
                type : "GET",
                url : confluenceContextPath + "/plugins/socialbookmarking/more.action" 
                
			});
			return false;
		});
	});
});

